//
//  ChatEntity.m
//  DChat
//
//  Created by ChenJunLi on 12/20/15.
//  Copyright © 2015 Elite. All rights reserved.
//

#import "ChatEntity.h"

@implementation ChatEntity

@synthesize _senderIdx;
@synthesize _senderName;
@synthesize _senderPhotoURL;
@synthesize _message;
@synthesize _revDate;
@synthesize _isRead;

- (instancetype) init {

    if(self = [super init]) {
        
        // initialize code here
        _senderIdx = -1;                // none value
        _senderName = @"";
        _senderPhotoURL = @"";
        _message = @"";
        _revDate = @"";
        _isRead = NO;
    }
    
    return self;
}

@end
