//
//  ChatEntity.h
//  DChat
//
//  Created by ChenJunLi on 12/20/15.
//  Copyright © 2015 Elite. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatEntity : NSObject {

    int _senderIdx;
    __weak NSString *_senderName;
    __weak NSString *_senderPhotoURL;
    __weak NSString *_message;
    __weak NSString *_revDate;
    
    BOOL _isRead;
}

@property (nonatomic) int _senderIdx;
@property (nonatomic, weak) NSString * _senderName;
@property (nonatomic, weak) NSString *_senderPhotoURL;
@property (nonatomic, weak) NSString *_message;
@property (nonatomic, weak) NSString *_revDate;

@property (nonatomic) BOOL _isRead;

@end
