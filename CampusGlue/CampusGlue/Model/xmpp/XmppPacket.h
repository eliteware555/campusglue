//
//  ;
//  DChat
//
//  Created by ChenJunLi on 12/22/15.
//  Copyright © 2015 Elite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

// chatting type
typedef enum {
    _TEXT = 0, _IMAGE, _VIDEO, _FILE
} ChatType;


///////////////////
//   room#roomJID#IMAGE#message#20151212 AM(PM), 12:00:00
////////////////////


@interface XmppPacket : NSObject {

    ChatType _chatType;                 // TEXT, IMAGE, VIDEO, FILE..
    NSString * _bodyString;             // text message or uploaded file url
    NSString * _fileName;               // upload file name(image, video, file transfer)
    NSString * _sentTime;               // sending time
    NSString * _from;                   // sender
    NSString * _roomName;               // chatting room name
    NSString * _participantName;        // participants name (chatting participants)
    
    NSString * _timestamp;              // timestamp of incoming message
    NSString * _date;
    
    BOOL _isDelayed;                    // delayed message (true)
    BOOL _isNew;                        // new receive message

    BOOL _bShowProgress;                // file uploading progress
    int _progressValue;                 // file uploading progress
    
    CGFloat height;
}


@property (nonatomic) ChatType _chatType;
@property (nonatomic, strong) NSString * _bodyString;
@property (nonatomic, strong) NSString * _sentTime;
@property (nonatomic, strong) NSString * _roomName;
@property (nonatomic, strong) NSString * _participantName;
@property (nonatomic, strong) NSString * _from;
@property (nonatomic, strong) NSString * _fileName;
@property (nonatomic, strong) NSString * _timestamp;
@property (nonatomic, strong) NSString * _date;
@property (nonatomic) BOOL _isDelayed;
@property (nonatomic) BOOL _isNew;
@property (nonatomic) BOOL _bShowProgress;
@property (nonatomic) int _progressValue;
@property (nonatomic) CGFloat height;

- (instancetype) initWithType:(NSString *)roomName participantName:(NSString *) participantName senderName:(NSString *) senderName chatType:(ChatType) chatType body:(NSString *) bodyString fileName:(NSString *)fileName sendTime:(NSString *) sendTime;

- (NSString *) toStringWithProtocol;
- (XmppPacket *) fromStringWithProtocol:(NSString *) message from:(NSString *) sender;
- (int) getParticipantsCount;



@end


