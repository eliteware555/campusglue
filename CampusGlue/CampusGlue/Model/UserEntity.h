//
//  UserEntity.h
//  CampusGlue
//
//  Created by victory on 3/18/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "RoomEntity.h"
#import "XmppPacket.h"

@interface UserEntity : NSObject

// User Personal Info
@property (nonatomic) int _idx;
@property (nonatomic, strong) NSString *_firstName;
@property (nonatomic, strong) NSString *_lastName;
@property (nonatomic, strong) NSString *_email;
@property (nonatomic, strong) NSString *_password;
@property (nonatomic, strong) NSString *_photoUrl;
@property (nonatomic) int _major;
@property (nonatomic) int _year;
@property (nonatomic) int _sex;
@property (nonatomic) int _status;
@property (nonatomic) int _interest;
@property (nonatomic, strong) NSString *_favMovie;
@property (nonatomic, strong) NSString *_aboutMe;
@property (nonatomic) int _friendCount;

// User Firend List & Chat Room List
@property (nonatomic, strong) NSMutableArray *_friendList;
@property (nonatomic, strong) NSMutableArray *_roomList;

@property (nonatomic, strong) NSMutableArray *_blockList;   // blocked user list

// not read message count
@property (nonatomic) int notReadCount;

// check the user validation with id
- (BOOL) isValid;

// return user's full name (first_name last_name)
- (NSString *) getFullName;

// add or delete friend
- (void) addFriend:(UserEntity *) _other;
- (void) deleteFriend:(UserEntity *) _other;

- (BOOL) isEqual:(UserEntity *)object;

// check if a user is friend with param _user
- (BOOL) isExistFriend:(UserEntity *)_other;

// get friend with idx
- (UserEntity *) getFriend:(int) idx;

// update room information with recieved packet
- (BOOL) updatedExistRoom:(XmppPacket *) _revPacket;

// update user's chat room list with received packet
- (void) updateUserRoomList:(XmppPacket *) _revPacket;

// add new room into user's chat room list
- (void) addRoomList:(RoomEntity *) _newRoom;

// return true if user is a blocked user, otherwise return false
- (BOOL) isBlockedUser: (int) _senderIdx;

@end






