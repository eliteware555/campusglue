//
//  AppDelegate.m
//  CampusGlue
//
//  Created by victory on 3/11/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "AppDelegate.h"
#import "CommonUtils.h"
#import "ReqConst.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize Me, xmpp, _deviceToken;
@synthesize chatVC, gTabbar;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [self initBaseView];
    
    // customize tab bar appearance
//    UIColor *backgroundColor = SelfColor(0, 40, 120);
    
    // set the bar background color
//    [[UITabBar appearance] setBackgroundImage:[AppDelegate imageFromColor:backgroundColor forSize:CGSizeMake(self.window.frame.size.width, 49) withCornerRadius:0]];
    
    // set the text color for selected state
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
    // set the text color for unselected state
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
   
    // Set the dark color to selected tab (the dimmed background)
    [[UITabBar appearance] setSelectionIndicatorImage:[AppDelegate imageFromColor:[UIColor blackColor] forSize:CGSizeMake(self.window.frame.size.width/4.0, 49) withCornerRadius:0]];
    
    // device token to receive push notification
    _deviceToken = @"";
    
    Me = [[UserEntity alloc] init];
    
    // initialize xmpp end point
    xmpp = [[XmppEndPoint alloc] initWithHostName:XMPP_SERVER_URL hostPort:5222];
    
    //
    // receive push message processing
    //
    if(launchOptions != nil) {
        
        NSDictionary * userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        
        if(userInfo != nil)
            [self application:application didReceiveRemoteNotification:userInfo];
    }
    
    //
    // Add registration for remote notifications (iOS 8 later....)
    //
    UIUserNotificationType allNotificationTypes =
    (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
    UIUserNotificationSettings *settings =
    [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    
    application.applicationIconBadgeNumber = 0;
    
    bEnteredBackground = NO;
    
    return YES;
}

//view for prevent touch event when loading
- (void) initBaseView {
    baseView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                        0,
                                                        [[UIScreen mainScreen] applicationFrame].size.width,
                                                        [[UIScreen mainScreen] applicationFrame].size.height)];

    [baseView setBackgroundColor:[UIColor blackColor]];
    baseView.userInteractionEnabled = YES;
    baseView.alpha = 0.05;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    //
    // register device token to openfire APNS Service
    //
    
    [xmpp registerDeviceToken:_deviceToken];
    
    //
    //  diconnect to xmpp
    //
    if(xmpp.xmppStream != nil)
    {
        // process the current chatting room
        if(xmpp.xmppJoinRoom != nil) {
            xmpp.xmppRoomJIDPaused = xmpp.xmppJoinRoom.myRoomJID;
            [xmpp leaveRoomInBg];
        } else {
            xmpp.xmppRoomJIDPaused = nil;
        }
        
        if(xmpp.isXmppConnected)
        {
            [xmpp disconnect];
        }
        
        [xmpp teardownStream];
    }
    
    bEnteredBackground = YES;
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    // recover xmpp connection
    if(xmpp.xmppStream == nil) {
        
        [xmpp setupStream];
    }
    
    if(bEnteredBackground) {
        
        if([Me isValid]) {
            if([xmpp connect:Me._idx password:Me._password] == false) {
                NSLog(@"xmpp connect failed.");
            }
        }
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

// --------------------------------------------------------------------------------------------------
#pragma mark APNS Callback
// --------------------------------------------------------------------------------------------------

- (void) application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

- (void) application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    NSString * token = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    _deviceToken = token;

    NSLog(@"This is device token = %@", _deviceToken);
}


- (void) application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    
    NSLog(@"Error %@", [error localizedDescription]);
}

- (void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    printf("did receive remote notification");
    
}

// --------------------------------------------------------------------------------------------------
#pragma mark Utils
// --------------------------------------------------------------------------------------------------

- (BOOL) connected  {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

- (void) showLoadingView:(id) sender
{
    UIViewController * _supver = (UIViewController *)sender;
    
    //self.view = baseView;
    [_supver.view addSubview:baseView];
    
    self.activityIndicator = [[PCAngularActivityIndicatorView alloc] initWithActivityIndicatorStyle:PCAngularActivityIndicatorViewStyleDefault];
    self.activityIndicator.color = [UIColor whiteColor];
    [_supver.view addSubview:self.activityIndicator];
    self.activityIndicator.center = _supver.view.center;
    
    [self.activityIndicator startAnimating];
}

- (void) hideLoadingView {
    
    [baseView removeFromSuperview];
    [self.activityIndicator stopAnimating];
}

- (void) hideLoadingView:(NSTimeInterval) delay {
    
    [NSTimer scheduledTimerWithTimeInterval:delay target:self selector:@selector(handleStop:) userInfo:nil repeats:NO];
}

- (void)handleStop:(id)sender
{
    [baseView removeFromSuperview];
    [self.activityIndicator stopAnimating];
}

- (void) showAlertDialog : (NSString *)title message:(NSString *) message positive:(NSString *)strPositivie negative:(NSString *) strNegative sender:(id) sender {
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    if(strPositivie != nil) {
        UIAlertAction * yesButton = [UIAlertAction
                                     actionWithTitle:strPositivie
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         //Handel your yes please button action here
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                     }];
        
        [alert addAction:yesButton];
    }
    
    if(strNegative != nil) {
        UIAlertAction * noButton = [UIAlertAction
                                    actionWithTitle:strPositivie
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        //Handel your yes please button action here
                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                    }];
        
        [alert addAction:noButton];
    }
    
    [sender presentViewController:alert animated:YES completion:nil];
    alert.view.tintColor = [UIColor colorWithRed:15/255.0 green:108/255.0 blue:36/255.0 alpha:1.0];
}

+ (UIImage *)imageFromColor:(UIColor *)color forSize:(CGSize)size withCornerRadius:(CGFloat)radius
{
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Begin a new image that will be the new image with the rounded corners
    // (here with the size of an UIImageView)
    UIGraphicsBeginImageContext(size);
    
    // Add a clip before drawing anything, in the shape of an rounded rect
    [[UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:radius] addClip];
    // Draw your image
    [image drawInRect:rect];
    
    // Get the image, here setting the UIImageView image
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    // Lets forget about that we were drawing
    UIGraphicsEndImageContext();
    
    return image;
}

- (void) notifyReceiveNewMessage {
    
    if(Me.notReadCount <= 0)
    {
        [gTabbar setBadgeStyle:kCustomBadgeStyleNone value:0 atIndex:1];
    } else
    {
        [gTabbar setBadgeStyle:kCustomBadgeStyleNumber value:Me.notReadCount atIndex:1];
    }
}

@end
