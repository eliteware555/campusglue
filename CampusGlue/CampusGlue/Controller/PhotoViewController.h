//
//  PhotoViewViewController.h
//  CampusGlue
//
//  Created by victory on 3/31/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoViewController : UIViewController

@property (nonatomic, strong) NSString *imageURL;

@end
