//
//  FriendProfileViewController.h
//  CampusGlue
//
//  Created by victory on 3/20/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserEntity.h"

@interface UserProfileViewController : UIViewController {
    
    UserEntity * selectedUser;
}

@property (nonatomic, strong) UserEntity *selectedUser;

@end
