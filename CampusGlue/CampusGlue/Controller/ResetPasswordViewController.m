//
//  ResetPasswordViewController.m
//  CampusGlue
//
//  Created by victory on 3/22/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "ResetPasswordViewController.h"
#import "CommonUtils.h"
#import "ReqConst.h"

@interface ResetPasswordViewController()

@property (nonatomic, weak) IBOutlet UIView *emailView;
@property (nonatomic, weak) IBOutlet UIView *verificationView;
@property (nonatomic, weak) IBOutlet UIView *passwordView;

@property (nonatomic, weak) IBOutlet UIButton *btnReset;
@property (nonatomic, weak) IBOutlet UIButton *btnSend;

@property (nonatomic, weak) IBOutlet UITextField *txtEmail;
@property (nonatomic, weak) IBOutlet UITextField *txtVerifyCode;
@property (nonatomic, weak) IBOutlet UITextField *txtPassword;

@end

@implementation ResetPasswordViewController

@synthesize txtEmail, txtVerifyCode, txtPassword;

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    [self initView];
}

- (void) initView {
    
    self.navigationController.navigationBar.hidden = NO;
    
    _emailView.layer.borderColor = SelfColor(255, 255, 255).CGColor;
    _verificationView.layer.borderColor = SelfColor(255, 255, 255).CGColor;
    _passwordView.layer.borderColor = SelfColor(255, 255, 255).CGColor;
    
    _btnSend.layer.borderColor = SelfColor(255, 255, 255).CGColor;
    _btnReset.layer.borderColor = SelfColor(255, 255, 255).CGColor;
}

- (void) didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
}

- (IBAction) sendAction:(id)sender {
    
    // check the validation of input value
//    if(txtEmail.)
}

- (IBAction) viewDidTapped:(id)sender {
    
    [self.view endEditing:YES];
}

// ---------------------------------------------
#pragma mark - UITextField Delegate
// ---------------------------------------------

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    if(txtVerifyCode == textField) {
        [txtPassword becomeFirstResponder];
    }
    
    [textField resignFirstResponder];
    
    return YES;
}

- (IBAction) resetAction:(id)sender {
    
    [self resetPassword];
}

- (IBAction) sendEmail:(id)sender {
    
    [self sendCode];
}

- (void) sendCode {
    
    if(txtEmail.text.length == 0) {
        return;
    }
    
    [APPDELEGATE showLoadingView:self];
    
    NSString *_email = [txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%@", SERVER_URL, REQ_GETAUTHCODE, _email];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        [APPDELEGATE hideLoadingView];
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            [[JLToast makeText:CHECK_CODE duration:2] show];
            
        } else if(nResult_Code == CODE_NOTEMAIL) {
            
            [APPDELEGATE showAlertDialog:nil message:UNREGISTERED_USER positive:ALERT_OK negative:nil sender:self];
            
        } else {
            [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [APPDELEGATE hideLoadingView];
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        
    }];
}

- (void) resetPassword {
    
    if(txtEmail.text.length == 0) {
        return;
    }
    
    if(txtVerifyCode.text.length == 0) {
        [APPDELEGATE showAlertDialog:nil message:INPUT_CODE positive:ALERT_OK negative:nil sender:self];
        return;
    }
    
    if(txtPassword.text.length == 0) {
        
        [APPDELEGATE showAlertDialog:nil message:INPUT_PWD positive:ALERT_OK negative:nil sender:self];
        return;
    }
    
    NSString *_email = [txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *_password = [txtPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *_verifyCode = [txtVerifyCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    [APPDELEGATE showLoadingView:self];
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%@/%@/%@", SERVER_URL, REQ_RESETPWD, _email, _password, _verifyCode];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        [APPDELEGATE hideLoadingView];
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            [[JLToast makeText:RESETPWD_SUCCESS duration:2] show];
            
        } else if(nResult_Code == CODE_NOTEMAIL) {
            
            [APPDELEGATE showAlertDialog:nil message:UNREGISTERED_USER positive:ALERT_OK negative:nil sender:self];
            
        } else if(nResult_Code == CODE_WRONGAUTH) {
            
            [APPDELEGATE showAlertDialog:nil message:WRONG_AUTH positive:ALERT_OK negative:nil sender:self];
            
        } else {
            
            [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [APPDELEGATE hideLoadingView];
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        
    }];
    
}




@end
