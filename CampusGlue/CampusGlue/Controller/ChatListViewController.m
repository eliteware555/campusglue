//
//  ChatListViewController.m
//  CampusGlue
//
//  Created by victory on 3/17/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "ChatListViewController.h"
#import "ChatViewController.h"
#import "SelectFriendViewController.h"
#import "CommonUtils.h"
#import "DirectoryCell.h"

@interface ChatListViewController() <UITableViewDataSource, UITableViewDelegate, DCMessageDelegate> {
    
    UserEntity *_user;
}

@property (nonatomic, weak) IBOutlet UITableView *tblChatList;

@end

@implementation ChatListViewController

@synthesize tblChatList;

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    [self initView];
}

- (void) initView {
    
    // initialize navigation bar
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.title = @"The University Of Texas - Arlington";
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:15.0], NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    tblChatList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    APPDELEGATE.xmpp._messageDelegate = self;
    
    _user = APPDELEGATE.Me;
    
    bIsShownChatList = YES;
    
    [tblChatList reloadData];
}

- (void) viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    bIsShownChatList = NO;
}

- (void) didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

// ---------------------------------------------------------------------------
#pragma mark - UITableViewDataSource
// ---------------------------------------------------------------------------
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    
    // return the number of sections
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in section
    return [_user._roomList count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * cellIdentifier = @"DirectoryCell";
    
    DirectoryCell *cell = (DirectoryCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    [cell setRoomModel:_user._roomList[indexPath.row]];
    
    return cell;
}

// edit tableview

- (BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return YES;
}

- (void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        RoomEntity *delRoom = _user._roomList[indexPath.row];
        
        // delete room from DB
        [[DBManager getSharedInstance] removeRoomWithName:delRoom._name];
        
        //
        [_user._roomList removeObject:delRoom];

        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 60;
}

- (void) deleteRoom {
    
}

// update the room information with new received packet
- (BOOL) updatedExistRoom:(XmppPacket *) _revPacket {
    
    for(RoomEntity * _entity in _user._roomList) {
        
        // update room info - unread message cnt and the lasted content
        // in case of inviting, participants, participants name and room display name
        if([_revPacket._roomName isEqualToString:_entity._name]) {
            
            // room content
            if(_revPacket._chatType == _TEXT) {
                _entity._recentContent = _revPacket._bodyString;
            } else {
                _entity._recentContent = @"Sent File";
            }
            
            // sending time and unread count
            _entity._recentDate = _revPacket._sentTime;
            _entity._recentCounter ++;
            
            // if room name is same with original, but participants are different, in case of inviting
            if(![_entity._participantsName isEqualToString:_revPacket._participantName]) {
                
                _entity._participantsName = _revPacket._participantName;
                
                [_entity updateParticipantWithPartName:_revPacket._participantName withBlock:^ {
                    
                    [self reloadData];
                }];
            }
            
            // in case of not change participants...
            [self reloadData];
            
            // update room database
            [[DBManager getSharedInstance] updateRoomWithName:_entity._name participant_name:_entity._participantsName recentMessage:_entity._recentContent recentTime:_entity._recentDate recetCounter:_entity._recentCounter];
            
//            _user.notReadCount ++;
            
            // notify unread message
//            [APPDELEGATE notifyReceiveNewMessage];
            
            return YES;
        }
    }    
    return NO;
}

// ------------------------------------
#pragma mark - DCMessage Delegate
// ------------------------------------
// update the room information or add new room with new received packet
- (void) newPacketReceived:(XmppPacket *)_revPacket {
    
    [CommonUtils vibrate];

    // check the user's room list & update
    // if user has not a room add it into user's room list
    if(![self updatedExistRoom:_revPacket]) {
        
        NSArray *ids = [_revPacket._participantName componentsSeparatedByString:@"_"];
        
        int other = 0;
        for(int j = 0; j < ids.count; j ++) {
            
            int idx = [ids[j] intValue];
            
            if (idx == _user._idx) {
                
                other = idx;
                break;
            }
        }

        // if user has not a room relation with new received packet, then add it into user's room lit
        NSMutableArray * _arrParticipants = [NSMutableArray array];
        NSString * url = [NSString stringWithFormat:@"%@%@/%@", SERVER_URL, REQ_GETUSERINFO, _revPacket._participantName];
        
        AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            
            NSLog(@"JSON: %@", responseObject);
            
            int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
            
            if(nResult_Code == CODE_SUCCESS) {
                
                NSDictionary *_dictParticipant = [responseObject objectForKey:RES_USERINFO];
                
                UserEntity *_entity = [[UserEntity alloc] init];
                _entity._idx = [[_dictParticipant valueForKey:RES_ID] intValue];
                
                _entity._firstName = [_dictParticipant valueForKey:RES_FIRSTNAME];
                _entity._lastName = [_dictParticipant valueForKey:RES_LASTNAME];
                
                _entity._major = [[_dictParticipant valueForKey:RES_MAJOR] intValue];
                _entity._year = [[_dictParticipant valueForKey:RES_YEAR] intValue];
                _entity._sex = [[_dictParticipant valueForKey:RES_SEX] intValue];
                _entity._status = [[_dictParticipant valueForKey:RES_STATUS] intValue];
                _entity._interest = [[_dictParticipant valueForKey:RES_INTEREST] intValue];
                
                _entity._favMovie = [_dictParticipant valueForKey:RES_MOVIE];
                _entity._aboutMe = [_dictParticipant valueForKey:RES_ABOUTME];
                _entity._photoUrl = [_dictParticipant valueForKey:RES_PHOTOURL];
                
                _entity._friendCount = [[_dictParticipant valueForKey:RES_FRIENDCOUNT] intValue];
                
                [_arrParticipants addObject:_entity];
                
                // make new room
                RoomEntity * _newRoom = [[RoomEntity alloc] initWithName:_revPacket._roomName participants:_arrParticipants];
                
                // update with new info
                _newRoom._recentContent = _revPacket._bodyString;
                
                _newRoom._recentDate = _revPacket._sentTime;
                _newRoom._recentCounter = 1;
                
                [_user addRoomList:_newRoom];
                
                [self reloadData];
            }
            
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            NSLog(@"Error: %@", error);
        }];
    }
}

- (void) reloadData {
    [tblChatList reloadData];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:@"SegueChatList2Chat"]) {

        // set chatting room
        
        NSIndexPath *selectedIndex = [self.tblChatList indexPathForSelectedRow];
        
        RoomEntity *_chatRoom = _user._roomList[selectedIndex.row];
        [APPDELEGATE.xmpp EnterChattingRoom:_chatRoom];
        
        ChatViewController * chatVC = (ChatViewController *)[segue destinationViewController];
        chatVC.chatRoom = _chatRoom;
        
    } else if([segue.identifier isEqualToString:@"SegueChatList2FriendsList"]) {
        
        SelectFriendViewController * selectFriendVC = (SelectFriendViewController *)[segue destinationViewController];
        selectFriendVC.fromWhere = FROM_CHATLIST;
    }
}

@end
