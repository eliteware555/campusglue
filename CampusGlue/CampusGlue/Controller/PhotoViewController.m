//
//  PhotoViewViewController.m
//  CampusGlue
//
//  Created by victory on 3/31/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "PhotoViewController.h"
#import "CommonUtils.h"

@interface PhotoViewController ()

@property (nonatomic, weak) IBOutlet UIImageView *imvPhoto;

@end

@implementation PhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.imvPhoto setImageWithURL:[NSURL URLWithString:self.imageURL]];
}


- (IBAction) userDidTapped:(id) sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
