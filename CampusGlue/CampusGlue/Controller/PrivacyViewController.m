//
//  PrivacyViewController.m
//  CampusGlue
//
//  Created by victory on 4/11/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "PrivacyViewController.h"

@interface PrivacyViewController ()

@property (nonatomic, weak) IBOutlet UITextView *txvPrivacy;

@end

@implementation PrivacyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.txvPrivacy.contentInset = UIEdgeInsetsMake(-70, 0, 0, 0);
    
    [self.txvPrivacy scrollRangeToVisible:NSMakeRange(0, 0)];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
