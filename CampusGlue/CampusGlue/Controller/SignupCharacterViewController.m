//
//  SignupStep2ViewController.m
//  CampusGlue
//
//  Created by victory on 3/13/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "SignupCharacterViewController.h"
#import "CommonUtils.h"
#import "UserEntity.h"
@import CoreActionSheetPicker;


@interface SignupCharacterViewController() {
    
    UserEntity *_user;
    
    NSMutableArray * _selectedIdx;
    NSArray * strIds;
}


@property (nonatomic, weak) IBOutlet UIView *firstNameView;
@property (nonatomic, weak) IBOutlet UIView *lastNameView;
@property (nonatomic, weak) IBOutlet UIView *sexView;
@property (nonatomic, weak) IBOutlet UIView *relationshipView;
@property (nonatomic, weak) IBOutlet UIView *interestedInView;

@property (nonatomic, weak) IBOutlet UIButton *btnMajor;
@property (nonatomic, weak) IBOutlet UIButton *btnYear;
@property (nonatomic, weak) IBOutlet UIButton *btnSex;
@property (nonatomic, weak) IBOutlet UIButton *btnrelation;
@property (nonatomic, weak) IBOutlet UIButton *btnInterested;

@property (nonatomic, weak) IBOutlet UIButton *btnNext;

@end

@implementation SignupCharacterViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [self initView];
    
    _user = APPDELEGATE.Me;
    
    _selectedIdx = [[NSMutableArray alloc] initWithArray:@[@-1, @-1, @-1, @-1, @-1, @-1]];
    strIds = @[SELECT_MAJOR, SELECT_YEAR, SELECT_SEX, SELECT_STATUS, SELECT_INTEREST];
}

-(void) initView {
    
    _firstNameView.layer.borderColor = SelfColor(255, 255, 255).CGColor;
    _lastNameView.layer.borderColor = SelfColor(255, 255, 255).CGColor;
    _sexView.layer.borderColor = SelfColor(255, 255, 255).CGColor;
    _relationshipView.layer.borderColor = SelfColor(255, 255, 255).CGColor;
    _interestedInView.layer.borderColor = SelfColor(255, 255, 255).CGColor;
    
    _btnNext.layer.borderColor = SelfColor(255, 255, 255).CGColor;
}

- (IBAction) majorSelction:(id)sender {
    
    [ActionSheetStringPicker showPickerWithTitle:@"MAJOR" rows:[CommonUtils getMajors] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        // set local variable with the selected value for implementing api
        _selectedIdx[0] = [NSNumber numberWithInt:(int)selectedIndex];
        [self.btnMajor setTitle:selectedValue forState:UIControlStateNormal];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {

    } origin:self.btnMajor];
}

- (IBAction) yearSelection:(id)sender {
    
    [ActionSheetStringPicker showPickerWithTitle:@"YEAR" rows:[CommonUtils getYears] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        // set local variable with the selected value for implementing api
        _selectedIdx[1] = [NSNumber numberWithInt:(int)selectedIndex];
        [self.btnYear setTitle:selectedValue forState:UIControlStateNormal];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {

    } origin:self.btnYear];
}

- (IBAction) sexSelection:(id)sender {
    
    [ActionSheetStringPicker showPickerWithTitle:@"SEX" rows:[CommonUtils getSexs] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        // set local variable with the selected value for implementing api
        _selectedIdx[2] = [NSNumber numberWithInt:(int)selectedIndex];
        [self.btnSex setTitle:selectedValue forState:UIControlStateNormal];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {

    } origin:self.btnSex];
}

- (IBAction) relationshipSelection:(id)sender {
    
    [ActionSheetStringPicker showPickerWithTitle:@"RELATIONSHIP STATUS" rows:[CommonUtils getStatus] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        // set local variable with the selected value for implementing api
        _selectedIdx[3] = [NSNumber numberWithInt:(int)selectedIndex];
        [self.btnrelation setTitle:selectedValue forState:UIControlStateNormal];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {

    } origin:self.btnrelation];
}

- (IBAction) interestSelection:(id)sender {
    
    [ActionSheetStringPicker showPickerWithTitle:@"INTERESTED IN" rows:[CommonUtils getInterest] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        // set local variable with the selected value for implementing api
        _selectedIdx[4] = [NSNumber numberWithInt:(int)selectedIndex];
        [self.btnInterested setTitle:selectedValue forState:UIControlStateNormal];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
    } origin:self.btnInterested];
}

- (IBAction) nextAction:(id)sender {   
    
    if([self checkValid]) {
        
        _user._major = [_selectedIdx[0] intValue];
        _user._year = [_selectedIdx[1] intValue];
        _user._sex = [_selectedIdx[2] intValue];
        _user._status = [_selectedIdx[3] intValue];
        _user._interest = [_selectedIdx[4] intValue];
        
        [self performSegueWithIdentifier:@"Segue2SignupProfile" sender:nil];
    }
}

- (BOOL) checkValid {
    
    for(int i = 0; i < 5; i ++) {
        
        if([_selectedIdx[i] intValue] == -1) {
            [APPDELEGATE showAlertDialog:nil message:strIds[i] positive:ALERT_OK negative:nil sender:self];
            return NO;
        }
    }
    
    return YES;
}

@end







