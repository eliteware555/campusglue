//
//  AddClassViewController.m
//  CampusGlue
//
//  Created by victory on 3/20/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "AddClassViewController.h"
#import "UserEntity.h"
#import "CommonUtils.h"
#import "ReqConst.h"

@interface AddClassViewController() <UITextFieldDelegate> {

    UserEntity *_user;
}

// course subject, number, section
@property (nonatomic, weak) IBOutlet UITextField *txtSubject;
@property (nonatomic, weak) IBOutlet UITextField *txtNumber;
@property (nonatomic, weak) IBOutlet UITextField *txtSection;

@end

@implementation AddClassViewController


- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    _user = APPDELEGATE.Me;
    
    [self initView];
}

- (void) initView {
    
    self.navigationItem.title = @"The University Of Texas - Arlington";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void) didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
}


// ------------------------------------------------------------------
#pragma  mark - UITextField Delegate
// ------------------------------------------------------------------

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    if(textField == _txtSubject) {
        [_txtNumber becomeFirstResponder];
    } else if(textField == _txtNumber) {
        [_txtSection becomeFirstResponder];
    }
    
    [textField resignFirstResponder];
    
    return YES;
}

- (IBAction) viewDidTapped:(id)sender {
    
    [self.view endEditing:YES];
}

- (IBAction) addClassAction:(id)sender {
    
    if(_txtSubject.text.length == 0 || _txtNumber.text.length == 0 || _txtSection.text.length == 0) {
        return;
    }
    
    [APPDELEGATE showLoadingView:self];
    
    NSString *classname = [self makeClassName];
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%d/%@", SERVER_URL, REQ_ADDCLASS, _user._idx, classname];
    NSString *  escapedURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:escapedURL parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        [APPDELEGATE hideLoadingView];
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            [[JLToast makeText:ADDCLASS_SUCCESS duration:2] show];            
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
    }];
    
}

- (NSString *) makeClassName {
    
    NSString *subject = [_txtSubject.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *number = [_txtNumber.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *section = [_txtSection.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *classname = [NSString stringWithFormat:@"%@ - %@ - %@", subject, number, section];
    
    classname = [classname uppercaseString];
    
    return classname;
}


@end
