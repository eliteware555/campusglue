//
//  AboutUsViewController.m
//  CampusGlue
//
//  Created by victory on 3/20/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "AboutUsViewController.h"

@interface AboutUsViewController()

@end

@implementation AboutUsViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    [self initView];
}

- (void) initView {
    
    self.navigationItem.title = @"The University Of Texas - Arlington";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void) didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
}

@end
