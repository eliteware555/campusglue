//
//  ChatListViewController.h
//  CampusGlue
//
//  Created by victory on 3/17/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatListViewController : UIViewController

- (void) reloadData;

@end
