//
//  ClassesViewController.m
//  CampusGlue
//
//  Created by victory on 3/20/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "ClassesViewController.h"
#import "ClassCell.h"
#import "UserEntity.h"
#import "ReqConst.h"
#import "CommonUtils.h"

@interface ClassesViewController() <UITableViewDelegate, UITableViewDataSource> {
    
    UserEntity *_user;
    NSMutableArray *classList;
}

@property (nonatomic, weak) IBOutlet UITableView *tblClasses;
@property (nonatomic, weak) IBOutlet UILabel *lblNote;

@end

@implementation ClassesViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    _user = APPDELEGATE.Me;
    classList = [[NSMutableArray alloc] init];
    
    [self initView];
}

- (void) initView {
    
    self.navigationItem.title = @"The University Of Texas - Arlington";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressed:)];
    longPress.minimumPressDuration = 1.0;
    [self.tblClasses addGestureRecognizer:longPress];
}

- (void) didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    self.lblNote.hidden = YES;
    
    [self getClasses];
}

- (IBAction) longPressed:(id)sender {
    
    UILongPressGestureRecognizer *longPress = (UILongPressGestureRecognizer *)sender;
    UIGestureRecognizerState state = longPress.state;
    
    CGPoint location = [longPress locationInView:self.tblClasses];
    NSIndexPath *indexPath = [self.tblClasses indexPathForRowAtPoint:location];
    
    if(state == UIGestureRecognizerStateBegan && indexPath != nil) {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"CampusGlue"
                                     message:@"Do you want to delete this class?"
                                     preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction * yesButton = [UIAlertAction
                                     actionWithTitle:ALERT_OK
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         //Handel your yes please button action here
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                         
                                         [self deleteClass:classList[indexPath.row]];
                                     }];
        [alert addAction:yesButton];

        UIAlertAction * noButton = [UIAlertAction
                                    actionWithTitle:ALERT_CANCEL
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        //Handel your yes please button action here
                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                    }];
        [alert addAction:noButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        alert.view.tintColor = [UIColor colorWithRed:15/255.0 green:108/255.0 blue:36/255.0 alpha:1.0];
    
        NSLog(@"Long Pressed : %d", (int)indexPath.row);
        return;
    }
}

// ---------------------------------------------------------------------------
#pragma mark - UITableViewDataSource
// ---------------------------------------------------------------------------
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    
    // return the number of sections
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in section
    return [classList count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * cellIdentifier = @"ClassCell";
    
    ClassCell *cell = (ClassCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.lblClassName.text = classList[indexPath.row];
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50;
}

- (void) getClasses {
    
    [APPDELEGATE showLoadingView:self];

    NSString * url = [NSString stringWithFormat:@"%@%@/%d", SERVER_URL, REQ_GETCLASS, _user._idx];
    NSString *  escapedURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:escapedURL parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        [APPDELEGATE hideLoadingView];
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            [classList removeAllObjects];
            
            NSArray * classes = [responseObject objectForKey:RES_CLASSNAME];
            
            for (int i = 0; i < [classes count]; i ++) {
                NSString *classname = classes[i];
                [classList addObject:classname];
            }
            
            if([classes count] == 0) {
                self.lblNote.hidden = NO;
            }
            
            [self.tblClasses reloadData];
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
    }];
}

- (void) deleteClass:(NSString *)_classname {
    
    [APPDELEGATE showLoadingView:self];
    
    NSString *todelete = [_classname uppercaseString];
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%d/%@", SERVER_URL, REQ_DELETECLASS, _user._idx, todelete];
    NSString *  escapedURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:escapedURL parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        [APPDELEGATE hideLoadingView];
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            [classList removeObject:_classname];
            
            if(classList.count == 0) {
                self.lblNote.hidden = NO;
            } else {
                self.lblNote.hidden = YES;
            }
            
            [self.tblClasses reloadData];
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
    }];
}

@end
