//
//  FriendProfileViewController.m
//  CampusGlue
//
//  Created by victory on 3/20/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "UserProfileViewController.h"
#import "CommonUtils.h"
#import "ChatViewController.h"
#import "ReqConst.h"
#import "FriendsViewController.h"
#import "RoomEntity.h"
#import "DCMessageDelegate.h"
#import "PhotoViewController.h"

@interface UserProfileViewController() <XmppFriendRequestDelegate> {

    UserEntity *_user;
    RoomEntity *_requestRoom;
}

@property (nonatomic, weak) IBOutlet UIImageView *imvProfile;
@property (nonatomic, weak) IBOutlet UILabel *lblTitleName;
@property (nonatomic, weak) IBOutlet UILabel *lblTitleMajor;

@property (nonatomic, weak) IBOutlet UIButton *btnAddFriend;
@property (nonatomic, weak) IBOutlet UIButton *btnMessage;

@property (nonatomic, weak) IBOutlet UIButton *btnFriends;
// account info
@property (nonatomic, weak) IBOutlet UILabel *lblName;
@property (nonatomic, weak) IBOutlet UILabel *lblUserID;

// academic info
@property (nonatomic, weak) IBOutlet UILabel *lblMajor;
@property (nonatomic, weak) IBOutlet UILabel *lblYear;

// personal info
@property (nonatomic, weak) IBOutlet UILabel *lblSex;
@property (nonatomic, weak) IBOutlet UILabel *lblRelationship;
@property (nonatomic, weak) IBOutlet UILabel *lblInterst;
@property (nonatomic, weak) IBOutlet UILabel *lblFavMovie;
@property (nonatomic, weak) IBOutlet UILabel *lblAboutMe;

@end

@implementation UserProfileViewController

@synthesize selectedUser;

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    [self initView];
}

- (void) initView {
    
    self.navigationItem.title = @"The University Of Texas - Arlington";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:15.0], NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    _imvProfile.layer.borderColor = [UIColor whiteColor].CGColor;
    _btnMessage.layer.borderColor = SelfColor(0, 153, 256).CGColor;
}

- (void) didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    // Me
    _user = APPDELEGATE.Me;
    
    APPDELEGATE.xmpp._friendRequestDelegate = self;
    
    [self updateUI];
}

- (void) updateUI {
    
    [_imvProfile setImageWithURL:[NSURL URLWithString:selectedUser._photoUrl] placeholderImage:[UIImage imageNamed:@"photo.png"]];
    _lblTitleName.text = [selectedUser getFullName];
    _lblTitleMajor.text = [NSString stringWithFormat:@"%@ Major", [CommonUtils getMajors][selectedUser._major]];
    
    _lblName.text = [NSString stringWithFormat:@"Name : %@", [selectedUser getFullName]];
    _lblUserID.text = [NSString stringWithFormat:@"User Number : %d", selectedUser._idx];
    
    _lblMajor.text = [NSString stringWithFormat:@"Major : %@", [CommonUtils getMajors][selectedUser._major]];
    _lblYear.text = [NSString stringWithFormat:@"Year : %@", [CommonUtils getYears][selectedUser._year]];
    
    _lblSex.text = [NSString stringWithFormat:@"Sex : %@", [CommonUtils getSexs][selectedUser._sex]];
    _lblRelationship.text = [NSString stringWithFormat:@"Relationship Status : %@", [CommonUtils getStatus][selectedUser._status]];
    _lblInterst.text = [NSString stringWithFormat:@"Interested In : %@", [CommonUtils getInterest][selectedUser._interest]];
    _lblFavMovie.text = [NSString stringWithFormat:@"Favorite Movie : %@", selectedUser._favMovie];
    _lblAboutMe.text = [NSString stringWithFormat:@"About me : %@", selectedUser._aboutMe];
    
    [_btnFriends setTitle:[NSString stringWithFormat:@"FRIENDS (%d)", selectedUser._friendCount] forState:UIControlStateNormal];    
    
    if([_user isExistFriend:selectedUser]) {
        
        [_btnAddFriend setTitle:@"UNFRIEND" forState:UIControlStateNormal];
    } else {
        
        [_btnAddFriend setTitle:@"ADD FRIEND" forState:UIControlStateNormal];
    }
}

// ----------------------------------------------------------------
#pragma mark - IBAction
// ----------------------------------------------------------------
- (IBAction) addOrUnFriendAction:(id)sender {
    
    if ([_btnAddFriend.titleLabel.text isEqualToString:@"ADD FRIEND"]) {
        [self addFriend];
    } else {
        [self unFriend];
    }
}

- (void) addFriend {
    
    [APPDELEGATE showLoadingView:self];
    
    // make server url
    NSString * url = [NSString stringWithFormat:@"%@%@/%d/%d", SERVER_URL, REQ_ADDFRIEND, _user._idx, selectedUser._idx];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {

        [APPDELEGATE hideLoadingView];
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            [_user addFriend:selectedUser];
            
            [[JLToast makeText:ADDFRIEND_SUCCESS duration:2.0] show];
            
            // change add friend button title with "UNFRIEND"
            [_btnAddFriend setTitle:@"UNFRIEND" forState:UIControlStateNormal];
            
            [self gotoAddFriendRoom];
            
        } else {
            
            [APPDELEGATE hideLoadingView];

            [APPDELEGATE showAlertDialog:nil message:ADDFRIEND_FAIL positive:ALERT_OK negative:nil sender:self];
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
    }];
}

// enter a room to sent a request
- (void) gotoAddFriendRoom {    
    
    _requestRoom = [self makeRoomWithFriend];
    
    // enter a room
    [APPDELEGATE.xmpp enterChattingRoomForFriendRequest:_requestRoom];
}

// friend request delegate
- (void) sendFriendRequest {
    
    XmppPacket *_sendPacket = [self makePacket:_TEXT send:[NSString stringWithFormat:@"%@ %@",_user._firstName, ADDFRIEND_MSG] sendFile:@""];
    
    [APPDELEGATE.xmpp sendPacket:_sendPacket];

    UserEntity *_participant = _requestRoom._participants[0];
    [APPDELEGATE.xmpp sendPacket:_sendPacket to:_participant._idx];
    
    APPDELEGATE.xmpp.isSentFriendRequest = NO;
}

// ----------------------------------------------
//  make sending / receiving packet
//  sending   : isSend - YES
//  receiving : isSend - NO
//
- (XmppPacket *) makePacket:(ChatType) _type send:(NSString *) _sendString sendFile:(NSString*) fileName {
    
    NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:currentDate]; // Get necessary date components
    
    [components month];     //  month
    [components day];       //  day
    [components year];      //  year
    
    NSString *  year_month_day = [NSString stringWithFormat:@"%d%02d%02d", (int)[components year], (int)[components month] , (int)[components day]];
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"H:mm:ss"];
    NSString * currentTime = [formatter stringFromDate:[NSDate date]];
    
    NSString * sendTime = [NSString stringWithFormat:@"%@,%@", year_month_day, currentTime];
    
    // ------------------------------
    // _from : sender idx
    XmppPacket *_sendPacket = [[XmppPacket alloc] initWithType:_requestRoom._name participantName:_requestRoom._participantsName senderName:[_user getFullName] chatType:_type body:_sendString fileName:fileName sendTime:sendTime];
    
    return _sendPacket;
}

- (RoomEntity *) makeRoomWithFriend {
    
    // make a chatting room with selected friend
    NSMutableArray *_friends = [NSMutableArray array];
    [_friends addObject:selectedUser];
    
    // new chatting room
    RoomEntity *_chatRoom = [[RoomEntity alloc] initWithParticipants:_friends];
    
    // check user's room list, if user has this room or not
    for(RoomEntity * _existEntity in _user._roomList) {
        
        if([_existEntity equals:_chatRoom]) {
            _chatRoom = _existEntity;
            return _chatRoom;
        }
    }
    
    XmppPacket *_sendPacket = [self makePacket:_TEXT send:[NSString stringWithFormat:@"%@ %@",_user._firstName, ADDFRIEND_MSG] sendFile:@""];
    
    _chatRoom._recentContent = _sendPacket._bodyString;
    _chatRoom._recentDate = _sendPacket._timestamp;
    
    // if user has not the current room, add it into user's room list
    [_user._roomList addObject:_chatRoom];
    
    // update db
    [[DBManager getSharedInstance] saveRoomWithName:_chatRoom._name participant_name:_chatRoom._participantsName recent_message:_chatRoom._recentContent recent_time:_chatRoom._recentDate recent_counter:_chatRoom._recentCounter];
    
    return _chatRoom;
}

- (void) unFriend {
    
    [APPDELEGATE showLoadingView:self];
    
    // make server url
    NSString * url = [NSString stringWithFormat:@"%@%@/%d/%d", SERVER_URL, REQ_DELETEFRIEND, _user._idx, selectedUser._idx];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        [APPDELEGATE hideLoadingView];
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            [_user deleteFriend:selectedUser];
            
            [[JLToast makeText:UNFRIEND_SUCCESS duration:2.0] show];
            
            // change add friend button title with "ADD FRIEND"
            [_btnAddFriend setTitle:@"ADD FRIEND" forState:UIControlStateNormal];
            
        } else {
            
            [APPDELEGATE showAlertDialog:nil message:UNFRIEND_FAIL positive:ALERT_OK negative:nil sender:self];
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
    }];
}

- (IBAction) messageWithUserAction:(id) sender {
    
    // pop up FriendProfileViewController and the push ChatViewController
    //get the existing navigationController view stack
    NSMutableArray* newViewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    
    //drop the last viewController and replace it with the new one!
    ChatViewController *chatVC = (ChatViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
    chatVC.hidesBottomBarWhenPushed = YES;
    [newViewControllers replaceObjectAtIndex:newViewControllers.count-1 withObject:chatVC];
    
    chatVC.chatRoom = [self makeRoomWithFriend];
    
    //set the new view Controllers in the navigationController Stack
    [self.navigationController setViewControllers:newViewControllers animated:YES];
}

- (IBAction) gotoFriendList:(id)sender {
    
    // check if the current user has friends or not
    // do not perform any thing if no friends
    
    // go to firend list
    if(selectedUser._friendCount == 0) {
        return;
    }
    
    [self performSegueWithIdentifier:@"SegueFriensProfile2Friends" sender:self];
}

- (IBAction) showProfileImage:(id)sender {
    
    PhotoViewController *photoViewVC = (PhotoViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
    
    photoViewVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    photoViewVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    
    photoViewVC.imageURL = selectedUser._photoUrl;
    
    [self presentViewController:photoViewVC animated:YES completion:nil];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:@"SegueFriensProfile2Friends"]) {
        
        FriendsViewController * friendsVC = (FriendsViewController *) [segue destinationViewController];
        friendsVC.selectedUser = self.selectedUser;
        friendsVC.fromWhere = FROM_USER_PROFILE;
    }
}

@end
