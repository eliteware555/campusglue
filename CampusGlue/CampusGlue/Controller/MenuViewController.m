//
//  MenuViewController.m
//  CampusGlue
//
//  Created by victory on 3/17/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "MenuViewController.h"
#import "AboutUsViewController.h"
#import "AddClassViewController.h"
#import "ClassmatesListViewController.h"
#import "CommonUtils.h"

@implementation MenuViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    [self initView];
}

- (void) initView {
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.title = @"The University Of Texas - Arlington";
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:15.0], NSForegroundColorAttributeName: [UIColor whiteColor]}];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
}

- (void) didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (IBAction) classClicked:(id)sender {
    
    [self performSegueWithIdentifier:@"Segue2Class" sender:self];
}

- (IBAction) classmatesClicked:(id)sender {
    
    [self performSegueWithIdentifier:@"Segue2ClassmatesList" sender:self];
}

- (IBAction) aboutusClicked:(id)sender {
    
    [self performSegueWithIdentifier:@"Segue2AboutUS" sender:self];
}

- (IBAction) logoutAction:(id)sender {
    
    // do something for logging out...
    [CommonUtils setIsFromLogout:YES];
    
    UINavigationController *loginNav = (UINavigationController *) [self.storyboard instantiateViewControllerWithIdentifier:@"LoginNav"];
    [[UIApplication sharedApplication].keyWindow setRootViewController:loginNav];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:@"Segue2AboutUS"]) {
        
        AboutUsViewController *aboutUsVC = (AboutUsViewController *)[segue destinationViewController];
        aboutUsVC.hidesBottomBarWhenPushed = YES;
    } else if ([segue.identifier isEqualToString:@"Segue2Class"]) {
        
        AddClassViewController *addClassVC = (AddClassViewController *)[segue destinationViewController];
        addClassVC.hidesBottomBarWhenPushed = YES;
    } else if([segue.identifier isEqualToString:@"Segue2ClassmatesList"]) {
        
        ClassmatesListViewController *classmatesListVC = (ClassmatesListViewController *)[segue destinationViewController];
        classmatesListVC.hidesBottomBarWhenPushed = YES;
    }
}


@end








