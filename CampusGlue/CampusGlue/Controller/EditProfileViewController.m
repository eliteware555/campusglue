//
//  EditProfileViewController.m
//  CampusGlue
//
//  Created by victory on 3/19/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "EditProfileViewController.h"
#import "CommonUtils.h"
@import CoreActionSheetPicker;
#import "UserEntity.h"
#import "ReqConst.h"
#import "NSString+Encode.h"

@interface EditProfileViewController() <UITextFieldDelegate, UITextViewDelegate> {
    
    UserEntity *_user;
    
    NSMutableArray * _selectedIdx;
    NSArray * strIds;
}

// account info
@property (nonatomic, weak) IBOutlet UITextField *txtFirstName;
@property (nonatomic, weak) IBOutlet UITextField *txtLastName;

// academic info
@property (nonatomic, weak) IBOutlet UILabel *lblMajor;
@property (nonatomic, weak) IBOutlet UIButton *btnMajor;
@property (nonatomic, weak) IBOutlet UILabel *lblYear;
@property (nonatomic,weak) IBOutlet UIButton *btnYear;

// personal info
@property (nonatomic, weak) IBOutlet UILabel *lblSex;
@property (nonatomic, weak) IBOutlet UIButton *btnSex;
@property (nonatomic, weak) IBOutlet UILabel *lblRelationshihp;
@property (nonatomic, weak) IBOutlet UIButton *btnRelation;
@property (nonatomic, weak) IBOutlet UILabel *lblInterestedin;
@property (nonatomic, weak) IBOutlet UIButton *btnInterstedIn;

@property (nonatomic, weak) IBOutlet UITextField *txtFavMovie;
@property (nonatomic, weak) IBOutlet UITextView *txvAboutMe;

@end

@implementation EditProfileViewController

@synthesize btnMajor, btnYear, btnSex, btnRelation, btnInterstedIn;

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    [self initVars];
    
    [self initView];
}

- (void) initVars {
    
    _user = APPDELEGATE.Me;
    
    _selectedIdx = [[NSMutableArray alloc] initWithArray:@[@-1, @-1, @-1, @-1, @-1, @-1]];
    strIds = @[SELECT_MAJOR, SELECT_YEAR, SELECT_SEX, SELECT_STATUS, SELECT_INTEREST];
    
    _selectedIdx[0] = [NSNumber numberWithInt:_user._major];
    _selectedIdx[1] = [NSNumber numberWithInt:_user._year];
    _selectedIdx[2] = [NSNumber numberWithInt:_user._sex];
    _selectedIdx[3] = [NSNumber numberWithInt:_user._status];
    _selectedIdx[4] = [NSNumber numberWithInt:_user._interest];
    
    NSLog(@"%d,%d,%d,%d,%d", [_selectedIdx[0] intValue], [_selectedIdx[1] intValue], [_selectedIdx[2] intValue], [_selectedIdx[3] intValue], [_selectedIdx[4] intValue]);
}

- (void) initView {
    
    self.navigationItem.title = @"The University Of Texas - Arlington";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.txtFirstName.text = _user._firstName;
    self.txtLastName.text = _user._lastName;
    
    self.lblMajor.text = [CommonUtils getMajors][_user._major];
    self.lblYear.text = [CommonUtils getYears][_user._year];
    self.lblSex.text = [CommonUtils getSexs][_user._sex];
    self.lblRelationshihp.text = [CommonUtils getStatus][_user._status];
    self.lblInterestedin.text = [CommonUtils getInterest][_user._interest];
    
    self.txtFavMovie.text = _user._favMovie;
    self.txvAboutMe.text = _user._aboutMe;
}

- (void) didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (IBAction) viewDiDTapped:(id) sender {
    
    [self.view endEditing:YES];
}

- (IBAction) majorSelction:(id)sender {
    
    [ActionSheetStringPicker showPickerWithTitle:@"MAJOR" rows:[CommonUtils getMajors] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        // set local variable with the selected value for implementing api
        _selectedIdx[0] = [NSNumber numberWithInt:(int)selectedIndex];
        self.lblMajor.text = selectedValue;
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        //        NSLog(@"major selction cancelled");
    } origin:self.btnMajor];
}

- (IBAction) yearSelection:(id)sender {
    
    [ActionSheetStringPicker showPickerWithTitle:@"YEAR" rows:[CommonUtils getYears] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        // set local variable with the selected value for implementing api
        _selectedIdx[1] = [NSNumber numberWithInt:(int)selectedIndex];
        self.lblYear.text = selectedValue;
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        //        NSLog(@"year selction cancelled");
    } origin:self.btnYear];
}

- (IBAction) sexSelection:(id)sender {
    
    [ActionSheetStringPicker showPickerWithTitle:@"SEX" rows:[CommonUtils getSexs] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        // set local variable with the selected value for implementing api
        _selectedIdx[2] = [NSNumber numberWithInt:(int)selectedIndex];
        self.lblSex.text = selectedValue;
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        //        NSLog(@"sex selction cancelled");
    } origin:self.btnSex];
}

- (IBAction) relationshipSelection:(id)sender {
    
    [ActionSheetStringPicker showPickerWithTitle:@"RELATIONSHIP STATUS" rows:[CommonUtils getSexs] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        // set local variable with the selected value for implementing api
        _selectedIdx[3] = [NSNumber numberWithInt:(int)selectedIndex];
        self.lblRelationshihp.text = selectedValue;
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        //        NSLog(@"relation selction cancelled");
    } origin:self.btnRelation];
}

- (IBAction) interestSelection:(id)sender {
    
    [ActionSheetStringPicker showPickerWithTitle:@"INTERESTED IN" rows:[CommonUtils getInterest] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        // set local variable with the selected value for implementing api
        _selectedIdx[4] = [NSNumber numberWithInt:(int)selectedIndex];
        self.lblInterestedin.text = selectedValue;
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        //        NSLog(@"Interest selction cancelled");
    } origin:self.btnInterstedIn];
}

// ------------------------------------------------------------------
#pragma  mark - UITextField Delegate
// ------------------------------------------------------------------

- (void) textFieldDidBeginEditing:(UITextField *)textField {
 
   if(textField == self.txtFavMovie) {
        
        [UIView animateWithDuration:0.3 animations:^{
            CGRect f = self.view.frame;
            f.origin.y -= 105;//(offsetY + deltaY);
            self.view.frame = f;
        }];
    }
}

- (void) textFieldDidEndEditing:(UITextField *)textField {
    
    if(textField == _txtFavMovie) {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect f = self.view.frame;
            f.origin.y = 0;//(offsetY + deltaY);
            self.view.frame = f;
        }];
    }
    
    
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    if(textField == _txtFirstName) {
        [_txtLastName becomeFirstResponder];
    }
    
    [textField resignFirstResponder];
    
    return YES;
}

// ------------------------------------------------------------------
#pragma  mark - UITextView Delegate
// ------------------------------------------------------------------

- (void) textViewDidBeginEditing:(UITextView *)textView {
    
    if(textView == _txvAboutMe) {
        
        [UIView animateWithDuration:0.3 animations:^{
            CGRect f = self.view.frame;
            f.origin.y -= 150;//(offsetY + deltaY);
            self.view.frame = f;
        }];
    }
    
}

- (void) textViewDidEndEditing:(UITextView *)textView {

    if(textView == _txvAboutMe) {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect f = self.view.frame;
            f.origin.y = 0;//(offsetY + deltaY);
            self.view.frame = f;
        }];
    }
}

- (IBAction) updateProfile:(id)sender {
    
    [self saveProfile];
}

- (void) saveProfile {
    
    NSString *_firstName = [self.txtFirstName.text encodeString:NSUTF8StringEncoding];
    
    NSString *_lastName = [self.txtLastName.text encodeString:NSUTF8StringEncoding];
    
    NSString *_favMovie = [self.txtFavMovie.text encodeString:NSUTF8StringEncoding];
    
    NSString *_aboutMe = [self.txvAboutMe.text encodeString:NSUTF8StringEncoding];
    
    // check the validation
    if(_firstName.length == 0) {
        
        [APPDELEGATE showAlertDialog:nil message:INPUT_FIRSTNAME positive:ALERT_OK negative:nil sender:self];
        return;
        
    } else if (_lastName.length == 0) {
        
        [APPDELEGATE showAlertDialog:nil message:INPUT_LASTNAME positive:ALERT_OK negative:nil sender:self];
        return;
        
    } else if(_favMovie.length == 0) {
        
        [APPDELEGATE showAlertDialog:nil message:SELECT_MOVIE positive:ALERT_OK negative:nil sender:self];
        return;
        
    } else if(_aboutMe.length == 0) {
        
        [APPDELEGATE showAlertDialog:nil message:SELECT_ABOUTME positive:ALERT_OK negative:nil sender:self];
        return;
    }
    
    
    [APPDELEGATE showLoadingView:self];

    NSString * url = [NSString stringWithFormat:@"%@%@/%d/%@/%@/%i/%i/%i/%i/%i/%@/%@", SERVER_URL, REQ_UPDATEPROFILE, _user._idx, _firstName, _lastName, [_selectedIdx[0] intValue], [_selectedIdx[1] intValue], [_selectedIdx[2] intValue], [_selectedIdx[3] intValue], [_selectedIdx[4] intValue], _favMovie, _aboutMe];
    
//    NSLog(@"%@", url);
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            [APPDELEGATE hideLoadingView];
            
            _user._firstName = self.txtFirstName.text;
            _user._lastName = self.txtLastName.text;
            
            _user._major = [_selectedIdx[0] intValue];
            _user._year = [_selectedIdx[1] intValue];
            _user._sex = [_selectedIdx[2] intValue];
            _user._status = [_selectedIdx[3] intValue];
            _user._interest = [_selectedIdx[4] intValue];
            
            _user._favMovie = _txtFavMovie.text;
            _user._aboutMe = _txvAboutMe.text;
            
            [[JLToast makeText:UPDATE_SUCCESS duration:2.0] show];
            
        } else {
            
            [APPDELEGATE hideLoadingView];
            [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        
        NSLog(@"Error: %@", error);
    }];
}

- (NSString *) encodeToPercentEscapeString:(NSString *)string {
    return (__bridge NSString *)
    CFURLCreateStringByAddingPercentEscapes(NULL,
                                            (CFStringRef) string,
                                            NULL,
                                            (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                            kCFStringEncodingUTF8);
}

@end





