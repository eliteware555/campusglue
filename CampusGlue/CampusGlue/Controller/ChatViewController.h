//
//  ChatViewController.h
//  CampusGlue
//
//  Created by victory on 3/21/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoomEntity.h"

@interface ChatViewController : UIViewController {
    
    RoomEntity *chatRoom;
}

@property (nonatomic, strong) RoomEntity *chatRoom;

@end
