//
//  TermsViewController.m
//  CampusGlue
//
//  Created by victory on 4/11/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "TermsViewController.h"

@interface TermsViewController ()

@property (nonatomic, weak) IBOutlet UITextView *txvTerms;

@end

@implementation TermsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.txvTerms.contentInset = UIEdgeInsetsMake(-70, 0, 0, 0);
    
    [self.txvTerms scrollRangeToVisible:NSMakeRange(0, 0)];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
