//
//  SelectFriendViewController.m
//  CampusGlue
//
//  Created by victory on 3/25/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "SelectFriendViewController.h"
#import "UserProfileViewController.h"
#import "UserEntity.h"
#import "CommonUtils.h"
#import "DirectoryCell.h"

@interface SelectFriendViewController () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate> {
 
    UserEntity * _user;
    
    NSMutableArray *friendsList;
    NSMutableArray *filteredFriendsList;
    
    BOOL searchActive;
}

@property (nonatomic, weak) IBOutlet UITableView *tblFriendsList;
@property (nonatomic, weak) IBOutlet UISearchBar *searchField;

@end

@implementation SelectFriendViewController

@synthesize fromWhere;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    friendsList = [[NSMutableArray alloc] init];
    filteredFriendsList = [[NSMutableArray alloc] init];
    
    _user = APPDELEGATE.Me;
    friendsList = _user._friendList;
    
    [self initView];
}

- (void) initView {
    
    self.navigationItem.title = @"The University Of Texas - Arlington";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:SelfColor(0, 40, 120)];
    
    // remove cell seperater for empty cells
    self.tblFriendsList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// ----------------------------------------------------------------------------------------------
#pragma mark - UISearchBarDelegate
// ----------------------------------------------------------------------------------------------
- (void) searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
    _searchField.showsCancelButton = YES;
}

- (void) searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    
    _searchField.showsCancelButton = NO;
}

- (void) searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [self.view endEditing:YES];
    
    searchBar.text = @"";
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    [self.view endEditing:YES];
}

- (void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    [filteredFriendsList removeAllObjects];
    
    if([searchText length] != 0) {
        
        searchActive = YES;
        [self searchTableList];
        
    } else {
        searchActive = NO;
    }
    
    [_tblFriendsList reloadData];
}

- (void)searchTableList {
    
    NSString *searchString = _searchField.text;
    
    for(UserEntity * _entity in friendsList) {
        
        NSComparisonResult result = [[_entity getFullName] compare:searchString options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchString length])];
        if (result == NSOrderedSame) {
            [filteredFriendsList addObject:_entity];
        }
    }
}

// ---------------------------------------------------------------------------
#pragma mark - UITableViewDataSource
// ---------------------------------------------------------------------------
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    
    // return the number of sections
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in section
    
    if(searchActive) {
        
        return filteredFriendsList.count;
    } else {
        
        return friendsList.count;
    }
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * cellIdentifier = @"DirectoryCell";
    DirectoryCell *cell = (DirectoryCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(searchActive) {
        
        [cell setUserModel:filteredFriendsList[indexPath.row]];
    } else {
        
        [cell setUserModel:friendsList[indexPath.row]];
    }
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 60;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(fromWhere == FROM_MY_PROFILE) {
        
        [self performSegueWithIdentifier:@"SegueMyFriendList2FriendProfile" sender:self];
        
    } else if(fromWhere == FROM_CHATLIST) {
        
        if(searchActive) {
            
            [self makeRoomWithFriend:filteredFriendsList[indexPath.row]];
        } else {
            
            [self makeRoomWithFriend:friendsList[indexPath.row]];
        }
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void) makeRoomWithFriend: (UserEntity *) participant {
    
    // make a chatting room with selected friend
    NSMutableArray *_friends = [NSMutableArray array];
    [_friends addObject:participant];
    
    // new chatting room
    RoomEntity *_chatRoom = [[RoomEntity alloc] initWithParticipants:_friends];
    
    // check user's room list, if user has this room or not
    for(RoomEntity * _existEntity in _user._roomList) {
        
        if([_existEntity equals:_chatRoom]) {
            
            _chatRoom = _existEntity;
            return;
        }
    }
    
    // if user has not the current room, add it into user's room list
    [_user._roomList addObject:_chatRoom];
    
    // update db
    [[DBManager getSharedInstance] saveRoomWithName:_chatRoom._name participant_name:_chatRoom._participantsName recent_message:_chatRoom._recentContent recent_time:_chatRoom._recentDate recent_counter:_chatRoom._recentCounter];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"SegueMyFriendList2FriendProfile"]) {
        
        NSIndexPath *selectedIndexPath = [_tblFriendsList indexPathForSelectedRow];
        UserEntity *_selectedUser;
        
        if(searchActive) {
            _selectedUser = filteredFriendsList[selectedIndexPath.row];
        } else {
            _selectedUser = friendsList[selectedIndexPath.row];
        }
        
        UserProfileViewController *userProfileVC = (UserProfileViewController *) [segue destinationViewController];
        userProfileVC.selectedUser = _selectedUser;
    }
}


@end
