//
//  ClassmatesViewController.h
//  CampusGlue
//
//  Created by victory on 3/20/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserEntity.h"

@interface FriendsViewController : UIViewController {
    
    int fromWhere;
    
    NSString *className;
    
    UserEntity *selectedUser;
}

@property (nonatomic, strong) UserEntity *selectedUser;
@property (nonatomic) int fromWhere;
@property (nonatomic, strong) NSString *className;

@end
