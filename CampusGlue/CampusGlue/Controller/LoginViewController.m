//
//  ViewController.m
//  CampusGlue
//
//  Created by victory on 3/11/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "LoginViewController.h"
#import "CommonUtils.h"
#import "SignupAccountViewController.h"
#import "ResetPasswordViewController.h"
#import "ReqConst.h"

#import "DBManager.h"

@interface LoginViewController () <UITextFieldDelegate> {
    
    UserEntity *_user;
    
    NSString *_email, *_password;
    
    int _nReqCounter;
}

@property (nonatomic, weak) IBOutlet UIView *emailView;
@property (nonatomic, weak) IBOutlet UIView *passwordView;
@property (nonatomic, weak) IBOutlet UIButton *btnSignup;
@property (nonatomic, weak) IBOutlet UITextField *txtEmail;
@property (nonatomic, weak) IBOutlet UITextField *txtPassword;

@end

@implementation LoginViewController

@synthesize txtEmail, txtPassword;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    
    // initialize variable
    _user = APPDELEGATE.Me;
    
    [self initView];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden = YES;
    
//    NSLog(@"is from logout: %@", [CommonUtils isFromLogout] ? @"YES" : @"NO");
    
    // when user logged out
    if([CommonUtils getIsFromLogout]) {
        // clear user email and password from userdefault
        
        [CommonUtils setUserPassword: @""];
        [CommonUtils setUserEmail: @""];
        
        txtEmail.text = @"";
        txtPassword.text = @"";
        
    } else {
        // auto login process
        
        NSString *pref_email = [CommonUtils getUserEmail];
        NSString *pref_password = [CommonUtils getUserPassword];
        
        // check the validation
        if(pref_email.length > 0 && pref_password > 0) {
            _email = pref_email;
            _password = pref_password;
            
            txtEmail.text = _email;
            txtPassword.text = _password;
            
            [self doLogin];
        }
    }
}

- (void) initView {
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    _emailView.layer.borderColor = SelfColor(255, 255, 255).CGColor;
    _passwordView.layer.borderColor = SelfColor(255, 255, 255).CGColor;
    _btnSignup.layer.borderColor = SelfColor(255, 255, 255).CGColor;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction) loginAction:(id)sender {
    
    if([self checkValid]) {
        
        _email = [txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        _password = [txtPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
      
        [self doLogin];
    }
}

- (IBAction) viewDidTapped:(id)sender {
    
    [self.view endEditing:YES];
    
//    [self.activityIndicator stopAnimating];
}

#pragma mark - UITextField Delegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    if(txtEmail == textField) {
        [txtPassword becomeFirstResponder];
    }
    
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL) checkValid {
    
    if(txtEmail.text.length == 0) {
        [APPDELEGATE showAlertDialog:nil message:INPUT_EMAIL positive:ALERT_OK negative:nil sender:self];
        return NO;
    } else if(txtPassword.text.length == 0) {
        [APPDELEGATE showAlertDialog:nil message:INPUT_PWD positive:ALERT_OK negative:nil sender:self];
    }
    
    return YES;
}

- (void) doLogin {

    [APPDELEGATE showLoadingView:self];
    
    // make server url
    NSString * url = [NSString stringWithFormat:@"%@%@/%@/%@", SERVER_URL, REQ_LOGIN, _email, _password];
    NSString *  escapedURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:escapedURL parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            NSDictionary * _dict = [responseObject objectForKey:RES_USERINFO];
            
            // set UserInfo : ME
            _user._idx = [[_dict valueForKey:RES_ID] intValue];
            
            _user._firstName = [_dict valueForKey:RES_FIRSTNAME];
            _user._lastName = [_dict valueForKey:RES_LASTNAME];
            
            _user._major = [[_dict valueForKey:RES_MAJOR] intValue];
            _user._year = [[_dict valueForKey:RES_YEAR] intValue];
            _user._sex = [[_dict valueForKey:RES_SEX] intValue];
            _user._status = [[_dict valueForKey:RES_STATUS] intValue];
            _user._interest = [[_dict valueForKey:RES_INTEREST] intValue];
            
            _user._favMovie = [_dict valueForKey:RES_MOVIE];
            _user._aboutMe = [_dict valueForKey:RES_ABOUTME];
            _user._photoUrl = [_dict valueForKey:RES_PHOTOURL];
            
            _user._friendCount = [[_dict valueForKey:RES_FRIENDCOUNT] intValue];
            
            _user._email = _email;
            _user._password = _password;
            [CommonUtils setUserEmail:_email];
            [CommonUtils setUserPassword:_password];
            [CommonUtils setXmppID:_user._idx];
            
            // init database if new user login
            if([CommonUtils getLastLoginEmail] == nil || ![[CommonUtils getLastLoginEmail] isEqualToString:_email]) {
                [CommonUtils setLastLoginEmail:_email];
                [[DBManager getSharedInstance] clearDB];
            }
            
            // extra
            [CommonUtils setIsFromLogout:NO];
            
            [self getFriends];
          
            
        } else if(nResult_Code == CODE_UNREGUSER){
            
            [APPDELEGATE hideLoadingView];
            
            [APPDELEGATE showAlertDialog:nil message:UNREGISTERED_USER positive:ALERT_OK negative:nil sender:self];
            
        } else if(nResult_Code == CODE_WRONGPWD) {
            
            [APPDELEGATE hideLoadingView];
            
            [APPDELEGATE showAlertDialog:nil message:CHECK_PWD positive:ALERT_OK negative:nil sender:self];
        } else {
            
            [APPDELEGATE hideLoadingView];
            [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
    }];
}

- (void) getFriends {

    if(_user._friendList != nil)
        [_user._friendList removeAllObjects];
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%i", SERVER_URL, REQ_GETFRIENDS, _user._idx];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            NSArray *_arrFriend = [responseObject objectForKey:RES_FRIENDLIST];
            
            for(NSDictionary *_dict in _arrFriend) {
                
                UserEntity *_entity = [[UserEntity alloc] init];
                
                _entity._idx = [[_dict valueForKey:RES_ID] intValue];
                _entity._firstName = [_dict valueForKey:RES_FIRSTNAME];
                _entity._lastName = [_dict valueForKey:RES_LASTNAME];

                _entity._major = [[_dict valueForKey:RES_MAJOR] intValue];
                _entity._year = [[_dict valueForKey:RES_YEAR] intValue];
                _entity._sex = [[_dict valueForKey:RES_SEX] intValue];
                _entity._status = [[_dict valueForKey:RES_STATUS] intValue];
                _entity._interest = [[_dict valueForKey:RES_INTEREST] intValue];
                
                _entity._favMovie = [_dict valueForKey:RES_MOVIE];
                _entity._aboutMe = [_dict valueForKey:RES_ABOUTME];
                _entity._photoUrl = [_dict valueForKey:RES_PHOTOURL];
                
                _entity._friendCount = [[_dict valueForKey:RES_FRIENDCOUNT] intValue];
                
                [_user._friendList addObject:_entity];
                
            }
        }
        
//        [self loadRoomInfo];
        
        [self getBlockList];
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [APPDELEGATE hideLoadingView];
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        
    }];
}

- (void) getBlockList {
    
    if(_user._blockList != nil) {
        [_user._blockList removeAllObjects];
    }
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%i", SERVER_URL, REQ_GETBLOCKLIST, _user._idx];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            NSArray *_arrFriend = [responseObject objectForKey:RES_BLOCKLIST];
            
            for( int i = 0; i < [_arrFriend count]; i ++) {
                
                [_user._blockList addObject:[NSNumber numberWithInt:[_arrFriend[i] intValue]]];
            }
            
//            [_user._blockList addObjectsFromArray:_arrFriend];
        }
        
        [self loadRoomInfo];
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [APPDELEGATE hideLoadingView];
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        
    }];
}

- (void) loadRoomInfo {
    
//    _user.notReadCount = 0;
    
    // initialize user's room list
    if(_user._roomList != nil)
        [_user._roomList removeAllObjects];
    
    NSArray * _arrRooms = [[DBManager getSharedInstance] loadRoom];
    
    _nReqCounter = (int)[_arrRooms count];
    
    if(_nReqCounter == 0) {
        
        [self loginToChattingServer];
        return;
    }
    
    for(int i = 0; i < [_arrRooms count]; i ++) {
        
        // get room name from database
        NSString * name = [[_arrRooms objectAtIndex:i] objectAtIndex:[[DBManager getSharedInstance].arrColumnNames indexOfObject:@"PARTICIPANTNAME"]];
        
        NSArray *ids = [name componentsSeparatedByString:@"_"];
        
        int other = 0;
        for(int j = 0; j < ids.count; j ++) {
            
            int idx = [ids[j] intValue];
            
            if (idx != _user._idx) {
                
                other = idx;
                break;
            }
        }
        
        // get participants info from a server
        NSMutableArray * _arrParticipants = [NSMutableArray array];
        NSString * url = [NSString stringWithFormat:@"%@%@/%d", SERVER_URL, REQ_GETUSERINFO, other];
        
        AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            
            int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
            
            if(nResult_Code == CODE_SUCCESS) {
                
                NSDictionary *_dictParticipant = [responseObject objectForKey:RES_USERINFO];
                
                UserEntity *_entity = [[UserEntity alloc] init];
                _entity._idx = [[_dictParticipant valueForKey:RES_ID] intValue];
                
                _entity._firstName = [_dictParticipant valueForKey:RES_FIRSTNAME];
                _entity._lastName = [_dictParticipant valueForKey:RES_LASTNAME];
                
                _entity._major = [[_dictParticipant valueForKey:RES_MAJOR] intValue];
                _entity._year = [[_dictParticipant valueForKey:RES_YEAR] intValue];
                _entity._sex = [[_dictParticipant valueForKey:RES_SEX] intValue];
                _entity._status = [[_dictParticipant valueForKey:RES_STATUS] intValue];
                _entity._interest = [[_dictParticipant valueForKey:RES_INTEREST] intValue];
                
                _entity._favMovie = [_dictParticipant valueForKey:RES_MOVIE];
                _entity._aboutMe = [_dictParticipant valueForKey:RES_ABOUTME];
                _entity._photoUrl = [_dictParticipant valueForKey:RES_PHOTOURL];
                
                _entity._friendCount = [[_dictParticipant valueForKey:RES_FRIENDCOUNT] intValue];
                
                [_arrParticipants addObject:_entity];
                
                // make a new room
                // set the room information with db data
                // recentContent, recenctDate, recentCounter
                NSString * roomName = [[_arrRooms objectAtIndex:i] objectAtIndex:[[DBManager getSharedInstance].arrColumnNames indexOfObject:@"NAME"]];
                NSString * recentContent = [[_arrRooms objectAtIndex:i] objectAtIndex:[[DBManager getSharedInstance].arrColumnNames indexOfObject:@"RECENTMESSAGE"]];
                NSString * recentDate = [[_arrRooms objectAtIndex:i] objectAtIndex:[[DBManager getSharedInstance].arrColumnNames indexOfObject:@"RECENTTIME"]];
                NSString * recentCounter = [[_arrRooms objectAtIndex:i] objectAtIndex:[[DBManager getSharedInstance].arrColumnNames indexOfObject:@"RECENTCOUNTER"]];
                
                RoomEntity *loadRoom = [[RoomEntity alloc] initWithName:roomName participants:_arrParticipants];
                
                loadRoom._recentContent = recentContent;
                loadRoom._recentDate = recentDate;
                loadRoom._recentCounter = [recentCounter intValue];
                
//                _user.notReadCount += loadRoom._recentCounter;
                
                [_user._roomList addObject:loadRoom];
            }
            
            if(--_nReqCounter <= 0) {
                
                [APPDELEGATE hideLoadingView];
                
                [self loginToChattingServer];
            }
            
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            
            if(--_nReqCounter <= 0) {
                
                [APPDELEGATE hideLoadingView];
                
                [self loginToChattingServer];
            }
            
            NSLog(@"Error: %@", error);
        }];
    }
}

- (void) loginToChattingServer {
    
    NSLog(@"User idx : %d", _user._idx);
    NSLog(@"User Password : %@", _user._password);
    
    if ([APPDELEGATE.xmpp connect:_user._idx password:_user._password])
    {
        // success to connect to xmpp server
        [self onSuccess];
    }
    else
    {
        // fail to connect to xmpp server
        [APPDELEGATE hideLoadingView];
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
    }
}

// go to dictionary
- (void) onSuccess {
    
    [APPDELEGATE hideLoadingView];
    
    [[DBManager getSharedInstance] updateChatNoCurrent];
    
    [self gotoDictionary];
}

- (void) gotoDictionary {
    
    UITabBarController *mainTab = (UITabBarController *) [self.storyboard instantiateViewControllerWithIdentifier:@"MainTabbar"];
    [[UIApplication sharedApplication].keyWindow setRootViewController:mainTab];
}


@end
