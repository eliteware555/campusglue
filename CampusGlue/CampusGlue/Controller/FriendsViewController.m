//
//  ClassmatesViewController.m
//  CampusGlue
//
//  Created by victory on 3/20/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "FriendsViewController.h"
#import "DirectoryCell.h"
#import "CommonUtils.h"
#import "ReqConst.h"
#import "UserProfileViewController.h"

@interface FriendsViewController() <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate> {
    
    NSMutableArray *friendsList;
    NSMutableArray *filteredFriendsList;
    
    BOOL searchActive;    
}

@property (nonatomic, weak) IBOutlet UITableView *tblFriendsList;

@property (nonatomic, weak) IBOutlet UISearchBar *searchField;

@end

@implementation FriendsViewController

@synthesize fromWhere, className;

@synthesize selectedUser;

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    friendsList = [[NSMutableArray alloc] init];
    filteredFriendsList = [[NSMutableArray alloc] init];
    
    searchActive = NO;
    
    [self initView];
}

- (void) initView {
    
    self.navigationItem.title = @"The University Of Texas - Arlington";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    // customize UISearchBar.
//    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setBackgroundColor:SelfColor(0, 60, 120)]; //(0, 51, 102)
//    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor whiteColor]];
//    [[UILabel appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor lightGrayColor]];
    
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:SelfColor(0, 40, 120)];
    
    // remove cell seperater for empty cells
    self.tblFriendsList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void) didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (void) viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    
    if(fromWhere == FROM_CLASSMATES) {
        
        [self getClassmates];
        
    } else {
        
        [self getFriends];
    }
}

// ----------------------------------------------------------------------------------------------
#pragma mark - UISearchBarDelegate
// ----------------------------------------------------------------------------------------------
- (void) searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
    _searchField.showsCancelButton = YES;
}

- (void) searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    
    _searchField.showsCancelButton = NO;
}

- (void) searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [self.view endEditing:YES];
    
    searchBar.text = @"";
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    [self.view endEditing:YES];
}

- (void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    [filteredFriendsList removeAllObjects];
    
    if([searchText length] != 0) {
        
        searchActive = YES;
        [self searchTableList];
        
    } else {
        searchActive = NO;
    }
    
    [_tblFriendsList reloadData];
}

- (void)searchTableList {
    
    NSString *searchString = _searchField.text;
    
    for(UserEntity * _entity in friendsList) {
        
        NSComparisonResult result = [[_entity getFullName] compare:searchString options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchString length])];
        if (result == NSOrderedSame) {
            [filteredFriendsList addObject:_entity];
        }
    }
}

// ---------------------------------------------------------------------------
#pragma mark - UITableViewDataSource
// ---------------------------------------------------------------------------
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    
    // return the number of sections
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in section
    
    if(searchActive) {
        
        return filteredFriendsList.count;
    } else {
        
        return friendsList.count;
    }
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * cellIdentifier = @"DirectoryCell";
    DirectoryCell *cell = (DirectoryCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(searchActive) {
        
        [cell setUserModel:filteredFriendsList[indexPath.row]];
    } else {
        
        [cell setUserModel:friendsList[indexPath.row]];
    }
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 60;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int _userIdx = APPDELEGATE.Me._idx;
    UserEntity * selectedFriend = friendsList[indexPath.row];
    
    if(_userIdx == selectedFriend._idx) {
        return;
    }
    
    // pop up FriendProfileViewController and the push ChatViewController
    //get the existing navigationController view stack
    NSMutableArray* newViewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    
    // remove friendsview controller
    [newViewControllers removeObjectAtIndex:newViewControllers.count - 1];
    
    UserProfileViewController *userProfileVC = (UserProfileViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
    
    if(searchActive) {
        userProfileVC.selectedUser = filteredFriendsList[indexPath.row];
    } else {
        userProfileVC.selectedUser = friendsList[indexPath.row];
    }

    [newViewControllers replaceObjectAtIndex:newViewControllers.count-1 withObject:userProfileVC];
    
    userProfileVC.hidesBottomBarWhenPushed = YES;
    //set the new view Controllers in the navigationController Stack
    [self.navigationController setViewControllers:newViewControllers animated:YES];
}

- (void) getFriends {
    
    [APPDELEGATE showLoadingView:self];
    
    // make server url
    NSString * url = [NSString stringWithFormat:@"%@%@/%i", SERVER_URL, REQ_GETFRIENDS, selectedUser._idx];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            [friendsList removeAllObjects];
            
            NSArray *_arrFriend = [responseObject objectForKey:RES_FRIENDLIST];
            
            for(NSDictionary *_dict in _arrFriend) {
                
                UserEntity *_entity = [[UserEntity alloc] init];
                
                _entity._idx = [[_dict valueForKey:RES_ID] intValue];
                _entity._firstName = [_dict valueForKey:RES_FIRSTNAME];
                _entity._lastName = [_dict valueForKey:RES_LASTNAME];
                
                _entity._major = [[_dict valueForKey:RES_MAJOR] intValue];
                _entity._year = [[_dict valueForKey:RES_YEAR] intValue];
                _entity._sex = [[_dict valueForKey:RES_SEX] intValue];
                _entity._status = [[_dict valueForKey:RES_STATUS] intValue];
                _entity._interest = [[_dict valueForKey:RES_INTEREST] intValue];
                
                _entity._favMovie = [_dict valueForKey:RES_MOVIE];
                _entity._aboutMe = [_dict valueForKey:RES_ABOUTME];
                _entity._photoUrl = [_dict valueForKey:RES_PHOTOURL];
                
                _entity._friendCount = [[_dict valueForKey:RES_FRIENDCOUNT] intValue];
                
                [friendsList addObject:_entity];
            }

            [self.tblFriendsList reloadData];
        }
        
        [APPDELEGATE hideLoadingView];
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [APPDELEGATE hideLoadingView];
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
    }];
}

- (void) getClassmates {
    
    int _userIdx = APPDELEGATE.Me._idx;
    
    [APPDELEGATE showLoadingView:self];

    NSString *_classname = [className stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    // make server url
    NSString * url = [NSString stringWithFormat:@"%@%@/%@", SERVER_URL, REQ_GETCLASSMATE, _classname];
    NSString *  escapedURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:escapedURL parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            [friendsList removeAllObjects];
            
            NSArray *_arrFriend = [responseObject objectForKey:RES_USERINFOS];
            
            for(NSDictionary *_dict in _arrFriend) {
                
                UserEntity *_entity = [[UserEntity alloc] init];
                
                _entity._idx = [[_dict valueForKey:RES_ID] intValue];
                _entity._firstName = [_dict valueForKey:RES_FIRSTNAME];
                _entity._lastName = [_dict valueForKey:RES_LASTNAME];
                
                _entity._major = [[_dict valueForKey:RES_MAJOR] intValue];
                _entity._year = [[_dict valueForKey:RES_YEAR] intValue];
                _entity._sex = [[_dict valueForKey:RES_SEX] intValue];
                _entity._status = [[_dict valueForKey:RES_STATUS] intValue];
                _entity._interest = [[_dict valueForKey:RES_INTEREST] intValue];
                
                _entity._favMovie = [_dict valueForKey:RES_MOVIE];
                _entity._aboutMe = [_dict valueForKey:RES_ABOUTME];
                _entity._photoUrl = [_dict valueForKey:RES_PHOTOURL];
                
                _entity._friendCount = [[_dict valueForKey:RES_FRIENDCOUNT] intValue];
                
                // except me
                if(_userIdx != _entity._idx) {
                    [friendsList addObject:_entity];
                }
            }
            
            [self.tblFriendsList reloadData];
        }
        
        [APPDELEGATE hideLoadingView];
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [APPDELEGATE hideLoadingView];
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
    }];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
//    if([segue.identifier isEqualToString:@"SegueFriends2UserProfile"]) {
//        
//        // set selectedFriend.
//        UserProfileViewController *userProfileVC = (UserProfileViewController *) [segue destinationViewController];
//        
//        NSIndexPath *selectedPath = [_tblFriendsList indexPathForSelectedRow];
//        
//        if(searchActive) {
//            userProfileVC.selectedUser = filteredFriendsList[selectedPath.row];
//        } else {
//            userProfileVC.selectedUser = friendsList[selectedPath.row];
//        }
//    }
}

@end
