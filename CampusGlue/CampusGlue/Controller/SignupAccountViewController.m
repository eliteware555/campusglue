//
//  SingupViewController.m
//  CampusGlue
//
//  Created by victory on 3/13/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "SignupAccountViewController.h"
#import "CommonUtils.h"
#import "UserEntity.h"
#import "ReqConst.h"
#import "NSString+Encode.h"
#import "M13Checkbox.h"
#import "ViewUtils.h"

@interface SignupAccountViewController() <UITextFieldDelegate> {
    
    UserEntity *_user;
    
    BOOL agreeOnTerms;
}


@property (nonatomic, weak) IBOutlet UIView *vTerms;

@property (nonatomic, weak) IBOutlet UIView *firstNameView;
@property (nonatomic, weak) IBOutlet UIView *lastNameView;
@property (nonatomic, weak) IBOutlet UIView *verificationView;
@property (nonatomic, weak) IBOutlet UIView *emailView;
@property (nonatomic, weak) IBOutlet UIView *passwordView;
@property (nonatomic, weak) IBOutlet UIButton *btnNext;
@property (nonatomic, weak) IBOutlet UIButton *btnSend;

@property (nonatomic, weak) IBOutlet UITextField *txtFirstName;
@property (nonatomic, weak) IBOutlet UITextField *txtLastName;
@property (nonatomic, weak) IBOutlet UITextField *txtEmail;
@property (nonatomic, weak) IBOutlet UITextField *txtPassword;
@property (nonatomic, weak) IBOutlet UITextField *txtVerificationCode;

@property (nonatomic, strong) M13Checkbox *chkTerms;

@end

@implementation SignupAccountViewController

@synthesize txtFirstName, txtLastName, txtEmail, txtPassword, txtVerificationCode;
@synthesize chkTerms;

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    [self initView];
    
    _user = APPDELEGATE.Me;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initView {
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    self.navigationController.navigationBar.hidden = NO;
    
    _firstNameView.layer.borderColor = SelfColor(255, 255, 255).CGColor;
    _lastNameView.layer.borderColor = SelfColor(255, 255, 255).CGColor;
    _verificationView.layer.borderColor = SelfColor(255, 255, 255).CGColor;
    _emailView.layer.borderColor = SelfColor(255, 255, 255).CGColor;
    _passwordView.layer.borderColor = SelfColor(255, 255, 255).CGColor;
    _btnNext.layer.borderColor = SelfColor(255, 255, 255).CGColor;
    _btnSend.layer.borderColor = SelfColor(255, 255, 255).CGColor;
    
    // checkbox for terms and Privacy Policy
    chkTerms = [[M13Checkbox alloc] initWithTitle:@"Agree to"];
    [chkTerms setCheckAlignment:M13CheckboxAlignmentLeft];
    chkTerms.strokeColor = [UIColor whiteColor];
    chkTerms.checkColor = [UIColor whiteColor];
    chkTerms.titleLabel.textColor = [UIColor whiteColor];
    chkTerms.tintColor = [UIColor clearColor];
    chkTerms.frame = CGRectMake(0, 8, chkTerms.frame.size.width, chkTerms.frame.size.height);
    [chkTerms addTarget:self action:@selector(checkChangedValue:) forControlEvents:UIControlEventValueChanged];
    
    [self.vTerms addSubview:chkTerms];
    
    // Terms
    NSString *strTerms = @"Terms &";
    UILabel *lblTerms = [[UILabel alloc] initWithFrame:CGRectMake(chkTerms.frame.size.width + 5, 8, 60, chkTerms.frame.size.height)];
    lblTerms.textColor = [UIColor whiteColor];
    
    NSDictionary *whiteBoldUnderLine = @{NSForegroundColorAttributeName :[UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:15.0], NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle)}; //
    
    NSMutableAttributedString *attributedTerms = [[NSMutableAttributedString alloc] initWithString:strTerms];
    [attributedTerms addAttributes:whiteBoldUnderLine range:NSMakeRange(0, strTerms.length-1)];
    lblTerms.attributedText = attributedTerms;
    lblTerms.userInteractionEnabled = YES;

    UITapGestureRecognizer *tapViewTerms = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTerms:)];
    tapViewTerms.numberOfTapsRequired = 1;

    [lblTerms addGestureRecognizer:tapViewTerms];
    
    [self.vTerms addSubview:lblTerms];
    
    // Privacy policy
    
    NSString *strPrivacy = @"Privacy Policy";
    CGFloat startX = lblTerms.origin.x + lblTerms.frame.size.width + 5;
    
    UILabel *lblPrivacy = [[UILabel alloc] initWithFrame:CGRectMake(startX, 8, self.vTerms.bounds.size.width - startX, chkTerms.frame.size.height)];
    lblPrivacy.textColor = [UIColor whiteColor];
    
    NSMutableAttributedString *attributedPrivacy = [[NSMutableAttributedString alloc] initWithString:strPrivacy];
    [attributedPrivacy addAttributes:whiteBoldUnderLine range:NSMakeRange(0, strPrivacy.length)];
    lblPrivacy.attributedText = attributedPrivacy;
    lblPrivacy.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tapViewPrivacy = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewPrivacy:)];
    tapViewPrivacy.numberOfTapsRequired = 1;
    
    [lblPrivacy addGestureRecognizer:tapViewPrivacy];
    
    [self.vTerms addSubview:lblPrivacy];
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}

- (void)checkChangedValue:(id)sender
{
    NSLog(@"Changed Value");
}

- (void) viewTerms:(UITapGestureRecognizer *) tapGesture {
    
    [self performSegueWithIdentifier:@"Segue2TermsofService" sender:self];    
}

- (void) viewPrivacy: (UITapGestureRecognizer *) tapGesture {
    
    [self performSegueWithIdentifier:@"Segue2Privacy" sender:self];
}

#pragma mark - UITextField Delegate

- (void) textFieldDidBeginEditing:(UITextField *)textField {
    
    if(txtPassword == textField) {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect f = self.view.frame;
            f.origin.y -= 45;//(offsetY + deltaY);
            self.view.frame = f;
        }];
    }
}

- (void) textFieldDidEndEditing:(UITextField *)textField {
    
    if (txtPassword == textField) {
        
        [UIView animateWithDuration:0.3 animations:^{
            CGRect f = self.view.frame;
            f.origin.y = 0.0;
            self.view.frame = f;
        }];
    }
    
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {

    if(txtFirstName == textField) {
        [txtLastName becomeFirstResponder];
    } else if (txtLastName == textField) {
        [txtEmail becomeFirstResponder];
    } else if (txtEmail == textField) {
        [txtVerificationCode becomeFirstResponder];
    } else if (txtVerificationCode == textField) {
        [txtPassword becomeFirstResponder];
    }
    
    [textField resignFirstResponder];
    
    return YES;
}

- (IBAction) nextAction:(id)sender {
    
    if([self checkValid]) {
        
        [self verifyEmail];
    }
}

- (BOOL) checkValid {
    
    if(txtFirstName.text.length == 0) {
        
        [APPDELEGATE showAlertDialog:nil message:INPUT_FIRSTNAME positive:ALERT_OK negative:nil sender:self];
        return NO;
    } else if (txtLastName.text.length == 0) {
        
        [APPDELEGATE showAlertDialog:nil message:INPUT_LASTNAME positive:ALERT_OK negative:nil sender:self];
        return NO;
    } else if(txtEmail.text.length == 0 || ![txtEmail.text hasSuffix:@"@mavs.uta.edu"]) {
        
        [APPDELEGATE showAlertDialog:nil message:INPUT_EMAIL positive:ALERT_OK negative:nil sender:self];
        return NO;
    } else if(txtVerificationCode.text.length == 0) {
        
        [APPDELEGATE showAlertDialog:nil message:INPUT_CODE positive:ALERT_OK negative:nil sender:self];
        return NO;
    } else if(txtPassword.text.length == 0) {
        
        [APPDELEGATE showAlertDialog:nil message:INPUT_PWD positive:ALERT_OK negative:nil sender:self];
        return NO;
    } else if (chkTerms.checkState != M13CheckboxStateChecked) {
        
        [APPDELEGATE showAlertDialog:nil message:READ_TERMS positive:ALERT_OK negative:nil sender:self];
        return NO;
    }
    
    return YES;
}

- (IBAction) sendCode:(id)sender {
    
    if(txtEmail.text.length == 0) {
        
        [[JLToast makeText:INPUT_EMAIL duration:3] show];
        return;
    }
    
    [APPDELEGATE showLoadingView:self];
    
    NSString *_email = [txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    // make server url
    NSString * url = [NSString stringWithFormat:@"%@%@/%@", SERVER_URL, REQ_GETVERIFYCODE, _email];
    NSString *  escapedURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:escapedURL parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        [APPDELEGATE hideLoadingView];
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            [[JLToast makeText:@"Please check your mail, and input the verification code." duration:3] show];
            
        } else if(nResult_Code == CODE_EXISTEMAIL) {
            
            [APPDELEGATE showAlertDialog:nil message:EMAIL_EXIST positive:ALERT_OK negative:nil sender:self];
            
        } else {
    
            [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        
        NSLog(@"Error: %@", error);
    }];
}

- (void) verifyEmail {
    
    [APPDELEGATE showLoadingView:self];
    
    NSString *_email = [txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *_code = [txtVerificationCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    // make server url
    NSString * url = [NSString stringWithFormat:@"%@%@/%@/%@", SERVER_URL, REQ_EMAILCONFIRM, _email, _code];
    NSString *  escapedURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:escapedURL parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        [APPDELEGATE hideLoadingView];
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            [self onSuccessVerify];

            
        } else if(nResult_Code == CODE_UNREGUSER) {

            [APPDELEGATE showAlertDialog:nil message:UNREGISTERED_USER positive:ALERT_OK negative:nil sender:self];
            
        } else if(nResult_Code == 120) {

            [APPDELEGATE showAlertDialog:nil message:WRONG_AUTH positive:ALERT_OK negative:nil sender:self];
            
        } else {
            
            [APPDELEGATE showAlertDialog:nil message:REGISTER_FAIL positive:ALERT_OK negative:nil sender:self];
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        
        NSLog(@"Error: %@", error);
    }];
}

- (void) onSuccessVerify {
    
    _user._firstName = [txtFirstName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _user._lastName = [txtLastName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _user._email = [txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _user._password = [txtPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    [self performSegueWithIdentifier:@"Segue2SignupCharacter" sender:nil];
}

@end

















