//
//  DictionaryViewController.m
//  CampusGlue
//
//  Created by victory on 3/17/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "DirectoryViewController.h"
#import "CommonUtils.h"
#import "UserEntity.h"
#import "UserProfileViewController.h"
#import "CarbonKit.h"
#import "ReqConst.h"
#import "DirectoryHeader.h"
#import "DirectoryCell.h"
#import "UIImage+ImageWithColor.h"
#import "UIScrollView+DXRefresh.h"

@interface DirectoryViewController() <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate> {
    
    BOOL searchActive;
    
    int _curPage;
    NSString *_curIndex;
    
    CarbonSwipeRefresh *refreshControl;
    
    NSMutableArray *userList;
    NSMutableArray *filteredUserList;
}

@property (nonatomic, weak) IBOutlet UISearchBar *searchField;
@property (nonatomic, weak) IBOutlet UITableView *tblUserList;

@end

@implementation DirectoryViewController

@synthesize tblUserList;

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    _curPage = 1;
    _curIndex = @"";
    
    userList = [[NSMutableArray alloc] init];
    filteredUserList = [[NSMutableArray alloc] init];
    
    searchActive = NO;
    
    [self initView];
}

- (void) initView {
    
    // initialize navigation bar
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.title = @"The University Of Texas - Arlington";
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:15.0], NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    // customize UISearchBar.
//    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setBackgroundColor:SelfColor(0, 60, 100)]; //(0, 51, 102)
//    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor whiteColor]];
//    [[UILabel appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor lightGrayColor]];
    
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:SelfColor(0, 40, 120)];
    
    // remove cell seperater for empty cells
    tblUserList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    // generate a tinted unselected image based on image passed via the storyboard
    for(UITabBarItem *item in self.tabBarController.tabBar.items) {
        // use the UIImage category code for the imageWithColor: method
        
        item.image = [item.selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    
    // refresh control
    refreshControl = [[CarbonSwipeRefresh alloc] initWithScrollView:self.tblUserList];
    [refreshControl setMarginTop:self.view.frame.size.height - 160];
    [refreshControl setColors:@[[UIColor blackColor], [UIColor blackColor], [UIColor blackColor]]];
    [self.view addSubview:refreshControl];
    
    [self.tblUserList addFooterWithTarget:self action:@selector(refreshFooter) withIndicatorColor:[UIColor darkGrayColor]];
    [self getAllUsers];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
}

- (void) didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (void)refreshFooter
{
    [self getAllUsers];
}


// ---------------------------------------------------------------------------
#pragma mark - UITableViewDataSource
// ---------------------------------------------------------------------------
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    
    // return the number of sections
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in section

    if(searchActive){
        
        return [filteredUserList count];
        
    } else {
        
        return [userList count];
    }
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * cellIdentifier =   @"DirectoryCell";
    static NSString * headerIdentifier = @"DirectoryHeader";
    
    if(searchActive) {
        
        if([filteredUserList[indexPath.row] isKindOfClass:[UserEntity class]]) {
            
            // return dictionary cell
            DirectoryCell * cell = (DirectoryCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            [cell setUserModel:filteredUserList[indexPath.row]];
            
            return cell;
            
        } else {
            
            // return dictionary header
            DirectoryHeader  * header = (DirectoryHeader *)[tableView dequeueReusableCellWithIdentifier:headerIdentifier];
            [header setHeader:filteredUserList[indexPath.row]];
            return header;
        }
        
    } else {
        
        if([userList[indexPath.row] isKindOfClass:[UserEntity class]]) {
            
            // return dictionary cell
            DirectoryCell * cell = (DirectoryCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            [cell setUserModel:userList[indexPath.row]];
            
            return cell;
            
        } else {
            
            // return dictionary header
            DirectoryHeader  * header = (DirectoryHeader *)[tableView dequeueReusableCellWithIdentifier:headerIdentifier];
            [header setHeader:userList[indexPath.row]];
            return header;
        }
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(searchActive) {    
        if([filteredUserList[indexPath.row] isKindOfClass:[UserEntity class]]) {
            return 60;
        } else {
            return 20;
        }
    } else {
        if([userList[indexPath.row] isKindOfClass:[UserEntity class]]) {
            return 60;
        } else {
            return 20;
        }
    }
}


// ----------------------------------------------------------------------------------------------
#pragma mark - UISearchBarDelegate
// ----------------------------------------------------------------------------------------------
- (void) searchBarTextDidBeginEditing:(UISearchBar *)searchBar {

    _searchField.showsCancelButton = YES;
}

- (void) searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    
    _searchField.showsCancelButton = NO;
}

- (void) searchBarCancelButtonClicked:(UISearchBar *)searchBar {

    [self.view endEditing:YES];
    _searchField.showsCancelButton = NO;
    
    searchBar.text = @"";

    if(searchActive) {
        searchActive = NO;
        [tblUserList reloadData];
    }
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar {

    [self.view endEditing:YES];
    _searchField.showsCancelButton = NO;
    
    if(searchBar.text.length > 0) {
        [self searchUsers];
    }
}

- (void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    if(searchActive && [searchText length] == 0) {
        searchActive = NO;
        [tblUserList reloadData];
    }
}

- (void) getAllUsers {
    
    [refreshControl startRefreshing];
    [self.tblUserList footerBeginRefreshing];
    
    // load userlist
    NSString * url = [NSString stringWithFormat:@"%@%@/%d", SERVER_URL, REQ_GETALLUSERS, _curPage];
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        [self.tblUserList footerEndRefreshing];
        [refreshControl endRefreshing];
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            NSArray *jsonUsers = [responseObject objectForKey:RES_USERINFOS];
            
            for(NSDictionary * _dict in jsonUsers) {
            
                UserEntity *jsonUser = [[UserEntity alloc] init];
                
                // set UserInfo : ME
                jsonUser._idx = [[_dict valueForKey:RES_ID] intValue];
                
                jsonUser._firstName = [_dict valueForKey:RES_FIRSTNAME];
                jsonUser._lastName = [_dict valueForKey:RES_LASTNAME];
                
                jsonUser._major = [[_dict valueForKey:RES_MAJOR] intValue];
                jsonUser._year = [[_dict valueForKey:RES_YEAR] intValue];
                jsonUser._sex = [[_dict valueForKey:RES_SEX] intValue];
                jsonUser._status = [[_dict valueForKey:RES_STATUS] intValue];
                jsonUser._interest = [[_dict valueForKey:RES_INTEREST] intValue];
                
                jsonUser._favMovie = [_dict valueForKey:RES_MOVIE];
                jsonUser._aboutMe = [_dict valueForKey:RES_ABOUTME];
                jsonUser._photoUrl = [_dict valueForKey:RES_PHOTOURL];
                
                jsonUser._friendCount = [[_dict valueForKey:RES_FRIENDCOUNT] intValue];
                
                // except me
                if (jsonUser._idx == APPDELEGATE.Me._idx) {
                    continue;
                }
                
                NSString *firstLetter = [[[jsonUser getFullName] substringToIndex:1] uppercaseString];
                
                if(_curPage == 0 || ![firstLetter isEqualToString:_curIndex]) {
                    
                    [userList addObject:firstLetter];
                    _curIndex = firstLetter;
                }
                
                [userList addObject:jsonUser];                
            }
            
            _curPage ++;
            [tblUserList reloadData];
            
        } else {
            
            [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        // stop refresh
        [self.tblUserList footerEndRefreshing];
        [refreshControl endRefreshing];
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
    }];
}

- (void) searchUsers {
    
    [APPDELEGATE showLoadingView:self];
    
    // load userlist
    NSString * url = [NSString stringWithFormat:@"%@%@/%@", SERVER_URL, REQ_SEARCHUSERS, [_searchField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    NSString *  escapedURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:escapedURL parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        [APPDELEGATE hideLoadingView];
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            NSArray *jsonUsers = [responseObject objectForKey:RES_SEARCHRESULT];
            
            [filteredUserList removeAllObjects];
            
            for(NSDictionary * _dict in jsonUsers) {
                
                UserEntity *jsonUser = [[UserEntity alloc] init];
                
                // set UserInfo : ME
                jsonUser._idx = [[_dict valueForKey:RES_ID] intValue];
                
                jsonUser._firstName = [_dict valueForKey:RES_FIRSTNAME];
                jsonUser._lastName = [_dict valueForKey:RES_LASTNAME];
                
                jsonUser._major = [[_dict valueForKey:RES_MAJOR] intValue];
                jsonUser._year = [[_dict valueForKey:RES_YEAR] intValue];
                jsonUser._sex = [[_dict valueForKey:RES_SEX] intValue];
                jsonUser._status = [[_dict valueForKey:RES_STATUS] intValue];
                jsonUser._interest = [[_dict valueForKey:RES_INTEREST] intValue];
                
                jsonUser._favMovie = [_dict valueForKey:RES_MOVIE];
                jsonUser._aboutMe = [_dict valueForKey:RES_ABOUTME];
                jsonUser._photoUrl = [_dict valueForKey:RES_PHOTOURL];
                
                jsonUser._friendCount = [[_dict valueForKey:RES_FRIENDCOUNT] intValue];
                
                // except me
                if (jsonUser._idx == APPDELEGATE.Me._idx) {
                    continue;
                }
                
                NSString *firstLetter = [[[jsonUser getFullName] substringToIndex:1] uppercaseString];
                
                if(_curIndex == 0 || ![firstLetter isEqualToString:_curIndex]) {
                    
                    [filteredUserList addObject:firstLetter];
                    _curIndex = firstLetter;
                }
                
                
                [filteredUserList addObject:jsonUser];
            }
            
            searchActive = YES;            
            [tblUserList reloadData];
            
        } else {
            
            [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        // stop refresh
        //        [refreshControl endRefreshing];
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
    }];
}


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.    
    if([segue.identifier isEqualToString:@"SegueDictionary2FriendProfile"]) {
        
        UserProfileViewController *userProfileVC = (UserProfileViewController *)[segue destinationViewController];
        userProfileVC.hidesBottomBarWhenPushed = YES;
        
        // set selectedFriend.
        NSIndexPath *selectedpath = [tblUserList indexPathForSelectedRow];
        
        if(searchActive) {
            userProfileVC.selectedUser = filteredUserList[selectedpath.row];
        } else {
            userProfileVC.selectedUser = userList[selectedpath.row];
        }        
    }
}

@end







