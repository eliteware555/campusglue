//
//  DCMessageDelegate.h
//  DChat
//
//  Created by ChenJunLi on 12/20/15.
//  Copyright © 2015 Elite. All rights reserved.
//

#ifndef DCMessageDelegate_h
#define DCMessageDelegate_h

#import <UIKit/UIKit.h>
#import "XmppPacket.h"

@protocol DCMessageDelegate <NSObject>

- (void) newPacketReceived:(XmppPacket *)_revPacket;

@end

@protocol DCRoomMessageDelegate <NSObject>

- (void) newRoomPackedReceived:(XmppPacket *) _revPacket;

@end


@protocol XmppCustomReconnectionDelegate <NSObject>

- (void) xmppConnected;
- (void) xmppDisconnected;

@end

@protocol XmppFriendRequestDelegate <NSObject>

- (void) sendFriendRequest;

@end

#endif /* DCMessageDelegate_h */
