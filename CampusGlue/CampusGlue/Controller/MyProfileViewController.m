//
//  ProfileViewController.m
//  CampusGlue
//
//  Created by victory on 3/17/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "MyProfileViewController.h"
#import "EditProfileViewController.h"
#import "SelectFriendViewController.h"
#import "UserEntity.h"
#import "CommonUtils.h"
#import "ReqConst.h"

@interface MyProfileViewController() <UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    
    UserEntity *_user;
    NSString *_photoPath;
}

@property (nonatomic, weak) IBOutlet UIImageView *imvProfile;
@property (nonatomic, weak) IBOutlet UILabel *lblTitleName;
@property (nonatomic, weak) IBOutlet UILabel *lblTitleMajor;

@property (nonatomic, weak) IBOutlet UIButton *btnEditProfile;

@property (nonatomic, weak) IBOutlet UIButton *btnFriends;
// account info
@property (nonatomic, weak) IBOutlet UILabel *lblName;
@property (nonatomic, weak) IBOutlet UILabel *lblUserID;

// academic info
@property (nonatomic, weak) IBOutlet UILabel *lblMajor;
@property (nonatomic, weak) IBOutlet UILabel *lblYear;

// personal info
@property (nonatomic, weak) IBOutlet UILabel *lblSex;
@property (nonatomic, weak) IBOutlet UILabel *lblRelationship;
@property (nonatomic, weak) IBOutlet UILabel *lblInterst;
@property (nonatomic, weak) IBOutlet UILabel *lblFavMovie;
@property (nonatomic, weak) IBOutlet UILabel *lblAboutMe;

@end

@implementation MyProfileViewController


- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    _user = APPDELEGATE.Me;
    
    [self initUI];
}

- (void) initUI {
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.title = @"The University Of Texas - Arlington";
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:15.0], NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    _imvProfile.layer.borderColor = [UIColor whiteColor].CGColor;
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self updateUI];
}

- (void) updateUI {
    
    [_imvProfile setImageWithURL:[NSURL URLWithString:_user._photoUrl] placeholderImage:[UIImage imageNamed:@"photo.png"]];
    _lblTitleName.text = [_user getFullName];
    _lblTitleMajor.text = [NSString stringWithFormat:@"%@ Major", [CommonUtils getMajors][_user._major]];
    
    _lblName.text = [NSString stringWithFormat:@"Name : %@", [_user getFullName]];
    _lblUserID.text = [NSString stringWithFormat:@"User Number : %d", _user._idx];
    
    _lblMajor.text = [NSString stringWithFormat:@"Major : %@", [CommonUtils getMajors][_user._major]];
    _lblYear.text = [NSString stringWithFormat:@"Year : %@", [CommonUtils getYears][_user._year]];
    
    _lblSex.text = [NSString stringWithFormat:@"Sex : %@", [CommonUtils getSexs][_user._sex]];
    _lblRelationship.text = [NSString stringWithFormat:@"Relationship Status : %@", [CommonUtils getStatus][_user._status]];
    _lblInterst.text = [NSString stringWithFormat:@"Interested In : %@", [CommonUtils getInterest][_user._interest]];
    _lblFavMovie.text = [NSString stringWithFormat:@"Favorite Movie : %@", _user._favMovie];
    _lblAboutMe.text = [NSString stringWithFormat:@"About me : %@", _user._aboutMe];
    
    [_btnFriends setTitle:[NSString stringWithFormat:@"FRIENDS (%d)", _user._friendCount] forState:UIControlStateNormal];
}

- (void) didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (IBAction) friendListAction :(id)sender {
    
    // check the number of the current user's friend
    // if he or she has no friend now, it will be returned.
    
    if(_user._friendCount == 0) {        
        return;
    }
    
    [self performSegueWithIdentifier:@"SegueProfile2FriendsList" sender:self];
}

// update user's profile pic
- (IBAction) updateProfile:(id)sender {
    
//    NSMutableAttributedString *attributedTitle = [[NSMutableAttributedString alloc] initWithString:@"Choose Image"];
//    [attributedTitle addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0, 12)];
//    [attributedTitle addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:24.0] range:NSMakeRange(0, 12)];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
//    [actionSheet setValue:attributedTitle forKey:@"attributedTitle"];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self openCamera];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self openGallery];
    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    actionSheet.view.tintColor = [UIColor lightGrayColor];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void) openCamera {
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    } else {
        
        NSLog(@"No Cameran\n. Please test on device");
    }
}

- (void) openGallery {
    
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController: imagePicker animated:YES completion:nil];
}

#pragma mark -
#pragma mark - UIImagePickerControllerDelegate

// This method is called when an image has been chosen from the album or taken from the camera
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage * chosenImage = info[UIImagePickerControllerEditedImage];
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
    [picker dismissViewControllerAnimated:YES completion:^{
        
        dispatch_queue_t writeQueue = dispatch_queue_create("SavePhoto", NULL);
        
        dispatch_async(writeQueue, ^{
            
            NSString * strPhotoPath = [CommonUtils saveToFile:chosenImage];
            
            dispatch_async(dispatch_get_main_queue(), ^ {
                
                _photoPath = strPhotoPath;
                
                [self uploadImage];
            });
        });
    }];
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void) uploadImage {
    
    [APPDELEGATE showLoadingView:self];
    
    // upload profile image
    NSString * url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_UPLOADIMAGE];
    NSDictionary *params = @{
                             PARAM_ID : [NSNumber numberWithInt:_user._idx]
                             };
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:[NSURL fileURLWithPath:_photoPath] name:@"file" fileName:@"filename.jpg" mimeType:@"image/jpg" error:nil];
    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:nil
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if (error) {
                          
                          NSLog(@"Error: %@", error);
                          
                          [APPDELEGATE hideLoadingView];
                          [APPDELEGATE showAlertDialog:nil message:PHOTO_UPLOAD_FAIL positive:ALERT_OK negative:nil sender:self];
                          
                      } else {
                          
                          NSLog(@"%@ %@", response, responseObject);
                          
                          [APPDELEGATE hideLoadingView];
                          
                          int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
                          
                          if(nResultCode == 0) {
                              
                              NSString *url = [responseObject valueForKey:RES_FILEURL];
                              
                              [self onSuccessUploadPhoto: url];
                              
                          } else {
                              
                              [APPDELEGATE hideLoadingView];
                              [APPDELEGATE showAlertDialog:nil message:PHOTO_UPLOAD_FAIL positive:ALERT_OK negative:nil sender:self];
                          }
                      }
                  }];
    
    [uploadTask resume];
}

- (void) onSuccessUploadPhoto:(NSString *) url {
    
    // update ui (set profile image with saved Photo URL
    [APPDELEGATE hideLoadingView];
    _user._photoUrl = url;
    [_imvProfile setImageWithURL:[NSURL URLWithString:_user._photoUrl] placeholderImage:[UIImage imageNamed:@"photo.png"]];
}


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:@"SegueProfile2FriendsList"]) {

        SelectFriendViewController *friendsVC = (SelectFriendViewController *)[segue destinationViewController];
        friendsVC.fromWhere = FROM_MY_PROFILE;
    }
}

@end
