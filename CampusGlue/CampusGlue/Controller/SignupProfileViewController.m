//
//  SignupProfileViewController.m
//  CampusGlue
//
//  Created by victory on 3/16/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "SignupProfileViewController.h"
#import "UITextView+Placeholder.h"
#import "CommonUtils.h"
#import "ReqConst.h"
#import "NSString+Encode.h"

@interface SignupProfileViewController() <UITextFieldDelegate, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    
    UserEntity *_user;
    NSString *_photoPath;
    
    int _idx;
}

@property (nonatomic, weak) IBOutlet UIView *viewPhoto;
@property (nonatomic, weak) IBOutlet UIView *viewFavoriteMovie;
@property (nonatomic, weak) IBOutlet UIView *viewAboutMe;

@property (nonatomic, weak) IBOutlet UITextField *txtFavoriteMovie;
@property (nonatomic, weak) IBOutlet UITextView *txvAboutMe;
@property (nonatomic, weak) IBOutlet UIButton *btnNext;

@property (nonatomic, weak) IBOutlet UIImageView *imvProfile;

@end

@implementation SignupProfileViewController

@synthesize txtFavoriteMovie, txvAboutMe;

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    [self initView];
    
    // initialize variable
    _user = APPDELEGATE.Me;
    _idx = 0;
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void) viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - keyboard will show
- (void)keyboardWillShow:(NSNotification *)notification {
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y -= 105;//(offsetY + deltaY);
        self.view.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification {
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0;
        self.view.frame = f;
    }];
}

- (void) initView {
    
    self.viewPhoto.layer.borderColor = [UIColor whiteColor].CGColor;
    self.viewFavoriteMovie.layer.borderColor = [UIColor whiteColor].CGColor;
    self.viewAboutMe.layer.borderColor = [UIColor whiteColor].CGColor;
    
    self.btnNext.layer.borderColor = [UIColor whiteColor].CGColor;
    
    txvAboutMe.placeholder = @"About Me(Less than 30 words)";
    txvAboutMe.placeholderColor = [UIColor whiteColor];
}

- (void) didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

#pragma mark - UITextField Delegate
- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    if(textField == txtFavoriteMovie) {
        [txvAboutMe becomeFirstResponder];
    }
    
    [textField resignFirstResponder];
    
    return YES;
}


- (IBAction) adddPhoto:(id)sender {
    
//    NSMutableAttributedString *attributedTitle = [[NSMutableAttributedString alloc] initWithString:@"Choose Image"];
//    [attributedTitle addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0, 12)];
//    [attributedTitle addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:24.0] range:NSMakeRange(0, 12)];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
//    [actionSheet setValue:attributedTitle forKey:@"attributedTitle"];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self openCamera];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self openGallery];
    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    actionSheet.view.tintColor = [UIColor lightGrayColor];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void) openCamera {
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    } else {
        
        NSLog(@"No Cameran\n. Please test on device");
    }
}

- (void) openGallery {
    
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController: imagePicker animated:YES completion:nil];
}

#pragma mark -
#pragma mark - UIImagePickerControllerDelegate

// This method is called when an image has been chosen from the album or taken from the camera
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage * chosenImage = info[UIImagePickerControllerEditedImage];
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
    [picker dismissViewControllerAnimated:YES completion:^{
        
        dispatch_queue_t writeQueue = dispatch_queue_create("SavePhoto", NULL);
        
        dispatch_async(writeQueue, ^{
            
            NSString * strPhotoPath = [CommonUtils saveToFile:chosenImage];
            
            dispatch_async(dispatch_get_main_queue(), ^ {
                
                _photoPath = strPhotoPath;
                
                // update ui (set profile image with saved Photo URL
                [self.imvProfile setImage:[UIImage imageWithContentsOfFile:strPhotoPath]];
            });
        });
    }];
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction) nextClicked:(id)sender {
    
    [self.view endEditing:YES];
    
    if([self checkValid]) {
        
        _user._favMovie = [txtFavoriteMovie.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        _user._aboutMe = [txvAboutMe.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        [self register];
    }    
}

- (IBAction) viewDidTapped:(id)sender {
    
    [self.view endEditing:YES];
}

- (BOOL) checkValid {
    
    if(_photoPath.length == 0) {
        [APPDELEGATE showAlertDialog:nil message:SELECT_PHOTO positive:ALERT_OK negative:nil sender:self];
        return NO;
    } else if(txtFavoriteMovie.text.length == 0) {
        [APPDELEGATE showAlertDialog:nil message:SELECT_MOVIE positive:ALERT_OK negative:nil sender:self];
        return NO;
    } else if(txvAboutMe.text.length == 0) {
        [APPDELEGATE showAlertDialog:nil message:SELECT_ABOUTME positive:ALERT_OK negative:nil sender:self];
        return NO;
    }
    
    return YES;
}

- (void) register {    
    
    [APPDELEGATE showLoadingView:self];
    
    NSString *_firstName = [_user._firstName encodeString:NSUTF8StringEncoding];
    
    NSString *_lastName = [_user._lastName encodeString:NSUTF8StringEncoding];
    
    NSString *_email = [_user._email encodeString:NSUTF8StringEncoding];
    
    NSString *_password = [_user._password encodeString:NSUTF8StringEncoding];
    
    NSString *_favMovie = [_user._favMovie encodeString:NSUTF8StringEncoding];
    
    NSString *_aboutMe = [_user._aboutMe encodeString:NSUTF8StringEncoding];
    
    // make server url
    NSString * url = [NSString stringWithFormat:@"%@%@/%@/%@/%@/%@/%i/%i/%i/%i/%i/%@/%@", SERVER_URL, REQ_SINGUP, _firstName, _lastName, _email, _password, _user._major, _user._year, _user._sex, _user._status, _user._interest, _favMovie, _aboutMe];


    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            _idx = [[responseObject valueForKey:RES_ID] intValue];

            // upload profile image
            [self uploadImage];
            
        } else if(nResult_Code == CODE_EXISTEMAIL) {

            [APPDELEGATE hideLoadingView];
            [APPDELEGATE showAlertDialog:nil message:EMAIL_EXIST positive:ALERT_OK negative:nil sender:self];
            
        } else {
            
            [APPDELEGATE hideLoadingView];
            [APPDELEGATE showAlertDialog:nil message:REGISTER_FAIL positive:ALERT_OK negative:nil sender:self];
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        
        NSLog(@"Error: %@", error);
    }];
}

- (void) uploadImage {
    
    // upload profile image
    NSString * url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_UPLOADIMAGE];
    NSDictionary *params = @{
                             PARAM_ID : [NSNumber numberWithInt:_idx]
                             };
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:[NSURL fileURLWithPath:_photoPath] name:@"file" fileName:@"filename.jpg" mimeType:@"image/jpg" error:nil];
    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:nil
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if (error) {
                          
                          NSLog(@"Error: %@", error);
                          
                          [APPDELEGATE hideLoadingView];
                          [APPDELEGATE showAlertDialog:nil message:PHOTO_UPLOAD_FAIL positive:ALERT_OK negative:nil sender:self];
                          
                      } else {
                          
                          NSLog(@"%@ %@", response, responseObject);
                          
                          [APPDELEGATE hideLoadingView];
                          
                          int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
                          
                          if(nResultCode == 0) {
                              
                              [self onSuccessRegister];
                              
                          } else {
                              
                              [APPDELEGATE showAlertDialog:nil message:PHOTO_UPLOAD_FAIL positive:ALERT_OK negative:nil sender:self];
                          }
                      }
                  }];
    
    [uploadTask resume];

}

- (void) onSuccessRegister {
    
    [CommonUtils setUserEmail:_user._email];
    [CommonUtils setUserPassword:_user._password];
    [CommonUtils setIsFromLogout: NO];
    
    // go to Login
    UINavigationController *loginNav = (UINavigationController *) [self.storyboard instantiateViewControllerWithIdentifier:@"LoginNav"];
    [[UIApplication sharedApplication].keyWindow setRootViewController:loginNav];
}

@end



