//
//  ChatViewController.m
//  CampusGlue
//
//  Created by victory on 3/21/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "ChatViewController.h"
#import "Inputbar.h"
#import "UIImage+ImageWithColor.h"
#import "DAKeyboardControl.h"
#import "CommonUtils.h"
#import "UserEntity.h"
#import "XmppPacket.h"
#import "ReceiveCell.h"
#import "SendCell.h"
#import "ChatHeader.h"
#import "Carbonkit.h"
#import "NSString+Utils.h"
#import "ReqConst.h"
#import "KxMenu.h"

#define DATE_HEADER_HEIGHT              30
#define MENU_WIDTH                      100

@interface ChatViewController() <InputbarDelegate, DCMessageDelegate, DCRoomMessageDelegate, UITableViewDelegate, UITableViewDataSource, XmppCustomReconnectionDelegate> {

    // message array
    NSMutableArray *messages;
    
    // refresh control & count
    CarbonSwipeRefresh *refreshControl;
    int refreshCount;
    
    UserEntity *_user;
    
    BOOL isChatAvailable;
    
    NSString *_lastReceivedDate;
}

@property (nonatomic, weak) IBOutlet Inputbar *inputbar;

@property (nonatomic, weak) IBOutlet UIBarButtonItem *btnBlockorUnblock;


@property (nonatomic, weak) IBOutlet UIView *vAddFriend;

// message table view container
@property (nonatomic, weak) IBOutlet UIView *vTblContainer;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *layout_height_inputbar;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *layout_bottom_inputbar;

@property (nonatomic, weak) IBOutlet UITableView *tblMessages;

@end

@implementation ChatViewController

// chatting room
@synthesize chatRoom;

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    [self initVars];
    
    [self initView];
}

- (void) initVars {
    
    messages = [[NSMutableArray alloc] init];
    
    _user = APPDELEGATE.Me;
    
    [self loadOldMessages];
    
    [self setMessageSendState: NO];
    
    APPDELEGATE.xmpp._messageDelegate = self;
    APPDELEGATE.xmpp._roomMessageDelegate = self;
    APPDELEGATE.xmpp._reconnectionDelegte = self;
}

- (void) initView {
    
    self.navigationItem.title = chatRoom._displayName;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:15.0], NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    // auto cell - size table view
    _tblMessages.estimatedRowHeight = 50;
    _tblMessages.rowHeight = UITableViewAutomaticDimension;
    
    // refresh control
    refreshControl = [[CarbonSwipeRefresh alloc] initWithScrollView:self.tblMessages];
    [refreshControl setMarginTop:0];
    [refreshControl setColors:@[[UIColor blackColor], [UIColor blackColor], [UIColor blackColor]]];
    [self.vTblContainer addSubview:refreshControl];
    
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    
    [self setUpInputbar];
    
    if([self checkNotRegisteredFriend]) {
        [self showAddFriendView];
    }
    
    // set title of right navigation bar button item
//    UserEntity *selecteduser = chatRoom._participants[0];
//    if([_user isBlockedUser:selecteduser._idx]) {
//        [self.btnBlockorUnblock setTitle:@"unblock"];
//    } else {
//        [self.btnBlockorUnblock setTitle:@"block"];
//    }
}

- (BOOL) checkNotRegisteredFriend {
    
    if(![_user isExistFriend:chatRoom._participants[0]]) {
        return YES;
    }    
    return NO;
}

- (void) didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    bIsShownChatView = YES;
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [[JLToast makeText:CHATTING_CONNECTING duration: 3] show];
}

- (void) viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

// ---------------------------------------------------------------------------
#pragma mark - Keyboard Notifications
// ---------------------------------------------------------------------------
- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary* notificationInfo = [notification userInfo];
    
    CGRect keyboardFrame = [[notificationInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    self.layout_bottom_inputbar.constant -= keyboardFrame.size.height;
    [UIView animateWithDuration:[[notificationInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue]
                          delay:0
                        options:[[notificationInfo valueForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue]
                     animations:^{
                         
                         [self.view layoutIfNeeded];
                         
                         [self tableViewScrollToBottomAnimated:YES];
                         
                     } completion:nil];
}

-(void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* notificationInfo = [notification userInfo];
    
    CGRect keyboardFrame = [[notificationInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    self.layout_bottom_inputbar.constant += keyboardFrame.size.height;
    self.layout_height_inputbar.constant = self.view.keyboardTriggerOffset;
    
    [UIView animateWithDuration:[[notificationInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue]
                          delay:0
                        options:[[notificationInfo valueForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue]
                     animations:^{
                         
                         [self.view layoutIfNeeded];
                         
                     } completion:nil];
}

// initialize inputbar
- (void) setUpInputbar {
   
    self.inputbar.rightButtonText = @"Send";
    self.inputbar.rightButtonTextColor = [UIColor whiteColor];
}

- (void) showAddFriendView {
 
    [UIView transitionWithView:self.vAddFriend duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:NULL completion:NULL];
    
//    [UIView transitionWithView:self.imvAddFriend duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:NULL completion:NULL];
    
    self.vAddFriend.hidden = NO;
//    self.imvAddFriend.hidden = NO;
}

- (void) hideAddFriendView {
    
    [UIView transitionWithView:self.vAddFriend duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:NULL completion:NULL];
//    
//    [UIView transitionWithView:self.imvAddFriend duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:NULL completion:NULL];
    
    self.vAddFriend.hidden = YES;
//    self.imvAddFriend.hidden = YES;
}

// load past messages from db
- (void) loadOldMessages {
    
    refreshCount = 1;
    NSMutableArray * recentMessages = [[DBManager getSharedInstance] loadMessageWithRoomName:chatRoom._name refreshCount:refreshCount];
    
    if(recentMessages.count > 0) {
        
        for(XmppPacket * _loadPacket in recentMessages) {
            
            _loadPacket._isNew = NO;
            [self addMessage:_loadPacket];
        }
    }
}

// load recent message from db and add messages array
// sort by date
- (void) loadRecentMessage {
    
    refreshCount ++;
    
    NSMutableArray * recentMessages = [[DBManager getSharedInstance] loadMessageWithRoomName:chatRoom._name refreshCount:refreshCount];
    
    if(recentMessages.count == 0) {
        refreshCount --;
        return;
    }
    
    NSString * _receivedDate = ((XmppPacket *)messages[1])._date;             // date
    XmppPacket * _firstPacket = recentMessages[0];
    
    int startIdx = 0;
    if([_firstPacket._date isEqualToString:_receivedDate]) {
        startIdx = 1;
    }
    
    for(XmppPacket * _loadPacket in recentMessages) {
        
        _loadPacket._isNew = NO;
        
        if(![_loadPacket._date isEqualToString:_receivedDate]) {
            
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"yyyyMMdd,H:mm:ss";
            NSDate *_revDate = [dateFormatter dateFromString:_loadPacket._timestamp];
            
            dateFormatter.dateStyle = NSDateFormatterLongStyle;
            
            [messages addObject:[dateFormatter stringFromDate:_revDate]];
            
            _receivedDate = _loadPacket._date;
            
            // Insert Message in UI (MessageTableView - ui_tblMsg)
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:messages.count-1 inSection:0];
            NSArray *indexPaths = [NSArray arrayWithObject:indexPath];
            
            [self.tblMessages beginUpdates];
            if(messages.count == 1)
                [self.tblMessages insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
            else
                [self.tblMessages insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationBottom];
            [self.tblMessages endUpdates];
        }
        
        // store message in the message array
        [messages insertObject:_loadPacket atIndex:startIdx];
        
        // Insert Message in UI (MessageTableView - ui_tblMsg)
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:startIdx++ inSection:0];
        NSArray *indexPaths = [NSArray arrayWithObject:indexPath];
        
        [self.tblMessages beginUpdates];
        if(messages.count == 0)
            [self.tblMessages insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
        else
            [self.tblMessages insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationBottom];
        [self.tblMessages endUpdates];
    }
}

/**
 **     add new message into message array
 **     update table and auto scroll to last indexpath
 **/
- (void) addMessage:(XmppPacket *) _revPacket {
    
    // add date
    if(messages.count == 0) {
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyyMMdd,H:mm:ss";
        NSDate *_revDate = [dateFormatter dateFromString:_revPacket._timestamp];
        
        dateFormatter.dateStyle = NSDateFormatterLongStyle;
        //        dateFormatter.doesRelativeDateFormatting = NO;
        
        [self.tblMessages beginUpdates];
        
        [messages addObject:[dateFormatter stringFromDate:_revDate]];
        
        _lastReceivedDate = _revPacket._date;
        
        // Insert Message in UI (MessageTableView - ui_tblMsg)
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:messages.count-1 inSection:0];
        NSArray *indexPaths = [NSArray arrayWithObject:indexPath];
        
        [self.tblMessages insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
        
        [self.tblMessages endUpdates];
        
    } else if(![_revPacket._date isEqualToString:_lastReceivedDate]) {
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyyMMdd,H:mm:ss";
        NSDate *_revDate = [dateFormatter dateFromString:_revPacket._timestamp];
        
        dateFormatter.dateStyle = NSDateFormatterLongStyle;
        
        _lastReceivedDate = _revPacket._date;
        
        [self.tblMessages beginUpdates];
        
        [messages addObject:[dateFormatter stringFromDate:_revDate]];
        
        _lastReceivedDate = _revPacket._date;
        
        // Insert Message in UI (MessageTableView - ui_tblMsg)
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:messages.count-1 inSection:0];
        NSArray *indexPaths = [NSArray arrayWithObject:indexPath];
        
        [self.tblMessages insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationBottom];
        [self.tblMessages endUpdates];
    }
    
    if(_revPacket._isNew) {
        [[DBManager getSharedInstance] saveChatWithRoomName:_revPacket._roomName content:[_revPacket toStringWithProtocol]  from:[_revPacket._from intValue] date:_revPacket._timestamp current:1];
    }
    
    // update chat list with new message info
    chatRoom._recentContent = _revPacket._bodyString;
        
    chatRoom._recentDate = _revPacket._sentTime;
    chatRoom._recentCounter = 0;
    
    // update room info
    [[DBManager getSharedInstance] updateRoomWithName:chatRoom._name participant_name:chatRoom._participantsName recentMessage:chatRoom._recentContent recentTime:_revPacket._sentTime recetCounter:0];
    
    [self.tblMessages beginUpdates];
    
    // store message in the message array
    [messages addObject:_revPacket];
    
    // Insert Message in UI (MessageTableView - ui_tblMsg)
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:messages.count-1 inSection:0];
    NSArray *indexPaths = [NSArray arrayWithObject:indexPath];
    
    [self.tblMessages insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationBottom];
    [self.tblMessages endUpdates];
    
    [self tableViewScrollToBottomAnimated:YES];
}

// scroll to bottom row of message table view
- (void)tableViewScrollToBottomAnimated:(BOOL)animated
{
    if(messages.count <= 0) return;
    
    double delay = 0.1 * NSEC_PER_SEC;
    dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, llrint(delay));
    dispatch_after(time, dispatch_get_main_queue(), ^{
        [self.tblMessages scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:messages.count-1 inSection:0]
                              atScrollPosition:UITableViewScrollPositionMiddle animated:animated];
    });
}

// -------------------------------------------------------------------------
// DCMessageDelegate - update participant with _revPacket
// -------------------------------------------------------------------------
- (void) newPacketReceived:(XmppPacket *)_revPacket {
    
    // update current chatting room ...
    // in case of changing of room participants...(invited another user by friend)
    if(![_revPacket._participantName isEqualToString:chatRoom._participantsName]) {
        
        chatRoom._participantsName = _revPacket._participantName;
        [chatRoom updateParticipantWithPartName:_revPacket._participantName withBlock:^{
            
            NSString * _title = chatRoom._displayName;
            [self.navigationItem setTitle:_title];
        }];
        
        // update room db.....
        [[DBManager getSharedInstance] updateRoomWithName:chatRoom._name participant_name:chatRoom._participantsName];
    }
}

// -------------------------------------------------------------------------
// DCRoomMessageDelegate
// -------------------------------------------------------------------------
- (void)  newRoomPackedReceived:(XmppPacket *)_revPacket {
    
    _revPacket._isNew = YES;
    [self addMessage:_revPacket];
    
    [CommonUtils vibrate];
}

// ---------------------------------------------------------------------------
#pragma mark - Inputbar delegate
// ---------------------------------------------------------------------------
-(void)inputbarDidPressRightButton:(Inputbar *)inputbar
{
    UserEntity *selecteduser = chatRoom._participants[0];
    
    if([_user isBlockedUser:selecteduser._idx]) {
        return;
    }
    
    NSString *messageStr =  [[self.inputbar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] substituteEmoticons];
    
    // receive - my message
    XmppPacket *_revPacket = [self makePacket:_TEXT send:messageStr sendFile:@"" sendType:NO];
    _revPacket._isNew = YES;
    [self addMessage:_revPacket];
    
    // send
    XmppPacket *_sendPacket = [self makePacket:_TEXT send:messageStr sendFile:@"" sendType:YES];
    [self sendPackeet:_sendPacket];
}

-(void)inputbarDidPressLeftButton:(Inputbar *)inputbar
{
    NSLog(@"Left Button Clicked");
}

-(void)inputbarDidChangeHeight:(CGFloat)new_height
{
    //Update DAKeyboardControl
    self.view.keyboardTriggerOffset = new_height;
    
    self.layout_height_inputbar.constant = new_height;
    [UIView animateWithDuration:0.5 animations:^{
        
        [self.view layoutIfNeeded];
        
        [self tableViewScrollToBottomAnimated:YES];
    }];
    
    //
}

- (void)refresh:(id)sender {
    
    NSLog(@"REFRESH");
    
    // do something here.
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self endRefreshing];
    });
}

- (void)endRefreshing {
    
    [self loadRecentMessage];
    
    [refreshControl endRefreshing];
}

//////////////////////////////////////////////////////
//  make a sending & receiving packet
//  sending   :isSend - YES
//  receiving :isSend - NO
/////////////////////////////////////////////////////
- (XmppPacket *) makePacket:(ChatType) _type send:(NSString *) _sendString sendFile:(NSString*) fileName sendType:(BOOL) isSend {
    
    NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:currentDate]; // Get necessary date components
    
    [components month];
    [components day];
    [components year];
    
    NSString *  year_month_day = [NSString stringWithFormat:@"%d%02d%02d", (int)[components year], (int)[components month] , (int)[components day]];
    
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"H:mm:ss"];
    NSString * currentTime = [formatter stringFromDate:[NSDate date]];
    
    NSString * sendTime = [NSString stringWithFormat:@"%@,%@", year_month_day, currentTime];
    
    /////////////
    // _from : sending   ----  sending user's name
    //       : receiving   ----  sender idx
    XmppPacket *_sendPacket = [[XmppPacket alloc] initWithType:chatRoom._name participantName:chatRoom._participantsName senderName:[_user getFullName] chatType:_type body:_sendString fileName:fileName sendTime:sendTime];
    
    if(!isSend) {
        
        XmppPacket * _revPacket = [[XmppPacket alloc] init];
        NSString * revMsg = [_sendPacket toStringWithProtocol];
        [_revPacket fromStringWithProtocol:revMsg from:[NSString  stringWithFormat:@"%d", _user._idx]];
        
        return _revPacket;
    }
    
    return _sendPacket;
}

- (void) sendPackeet:(XmppPacket *) _sendPacket {
    
    // room message
    [APPDELEGATE.xmpp sendPacket:_sendPacket];
    
    // outside message for all room participants
    for(UserEntity * _friend in chatRoom._participants) {
        [APPDELEGATE.xmpp sendPacket:_sendPacket to:_friend._idx];
    }
}

// ---------------------------------------------------------------------------
#pragma mark - UITableViewDataSource
// ---------------------------------------------------------------------------

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in section
    
    return [messages count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * receiveIdentifier = @"ReceiveCell";
    static NSString * sendIdentifier = @"SendCell";
    static NSString * headerIdentifier = @"DateHeader";
    
    if(![(messages[indexPath.row]) isKindOfClass:[XmppPacket class]]) {
        
        ChatHeader * cell = (ChatHeader *) [tableView dequeueReusableCellWithIdentifier:headerIdentifier];
        
        [cell setDate:messages[indexPath.row]];
        
        return cell;
        
    } else {
        
        XmppPacket * revPacket = messages[indexPath.row];
        int _sender = [revPacket._from intValue];
            
        if(_sender == _user._idx) { // sent by me
            
            SendCell * cell = (SendCell *) [tableView dequeueReusableCellWithIdentifier:sendIdentifier];
            [cell setMessage:revPacket];

            return cell;
        }
        else        // sent by friend
        {
            ReceiveCell * cell = (ReceiveCell *) [tableView dequeueReusableCellWithIdentifier:receiveIdentifier];
            [cell setMessage:revPacket];
            
            return cell;
        }
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(![(messages[indexPath.row]) isKindOfClass:[XmppPacket class]]) {
        
        return DATE_HEADER_HEIGHT;
        
    } else {
        return UITableViewAutomaticDimension;
    }
}

#pragma mark - xmpp custom reconnection delegate
// user can send message to hist friend
- (void) xmppConnected {

    [[JLToast makeText:CHATTING_SUCCCESS duration:2.0] show];
    [self setMessageSendState:YES];
}

// user can't send any message, he must wait when to reconnect to a server
// it wil be done automatically
- (void) xmppDisconnected {
    
    [[JLToast makeText:CHATTING_ERROR duration:2.0] show];
    [self setMessageSendState:NO];
}

- (void) setMessageSendState:(BOOL) _state {
    
    [self.inputbar setEditable:_state];
    //    _isChatAvailable = _state;
}

- (IBAction) addFriendAction:(id)sender {
    
    [self addFriend];
}

- (void) addFriend {
    
    [APPDELEGATE showLoadingView:self];
    
    UserEntity *selectedUser = chatRoom._participants[0];
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%d/%d", SERVER_URL, REQ_ADDFRIEND, _user._idx, selectedUser._idx];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        [APPDELEGATE hideLoadingView];
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            [_user addFriend:selectedUser];
            
            XmppPacket *_sendPacket = [self makePacket:_TEXT send:[NSString stringWithFormat:@"%@ %@",_user._firstName, ADDFRIEND_MSG] sendFile:@"" sendType:NO];
            
            _sendPacket._isNew = YES;
            [self addMessage:_sendPacket];
            
            [APPDELEGATE.xmpp sendPacket:_sendPacket];
            
            [[JLToast makeText:ADDFRIEND_SUCCESS duration:2.0] show];
            
            // hide add friend view
            [self hideAddFriendView];
            
        } else {
            
            [APPDELEGATE hideLoadingView];
            
            [APPDELEGATE showAlertDialog:nil message:ADDFRIEND_FAIL positive:ALERT_OK negative:nil sender:self];
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
    }];
}

// block or unblock user
// report user
- (IBAction) showActionMenu:(id)sender {
    
    const CGFloat W = self.view.bounds.size.width;
    CGRect menuFrame = CGRectMake(W - MENU_WIDTH - 5, 5, MENU_WIDTH, 50);
    
    UserEntity *selecteduser = chatRoom._participants[0];
//    if([_user isBlockedUser:selecteduser._idx]) {
//        [self.btnBlockorUnblock setTitle:@"unblock"];
//    } else {
//        [self.btnBlockorUnblock setTitle:@"block"];
//    }
    
    NSArray *menuItems =
    @[
      
      [KxMenuItem menuItem: [_user isBlockedUser:selecteduser._idx] ? MENU_UNBLOCK : MENU_BLOCK
                     image:nil
                    target:self
                    action:@selector(blockOrUnBlockUser:)],
      
      [KxMenuItem menuItem:MENU_REPORT
                     image:nil
                    target:self
                    action:@selector(reportUser:)],
      ];
    
    [KxMenu showMenuInView:self.view
                  fromRect:menuFrame
                 menuItems:menuItems];
}

- (void) reportUser: (id) sender {
    
    [APPDELEGATE showLoadingView:self];
    
    UserEntity *selectedUser = chatRoom._participants[0];
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%d/%d", SERVER_URL, REQ_REPORTUSER, _user._idx, selectedUser._idx];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        [APPDELEGATE hideLoadingView];
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {

            [[JLToast makeText:SUCCESS_REPORT duration:3] show];
            
        } else {
            
            [[JLToast makeText:FAIL_REPORT duration:3] show];
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
    }];
}

// block or unblock user
- (void) blockOrUnBlockUser:(id)sender {
    
    [APPDELEGATE showLoadingView:self];
    
    UserEntity *selectedUser = chatRoom._participants[0];
    
    int reqBlockStatus = REQ_BLOCKUSER;
    if([_user isBlockedUser:selectedUser._idx]) {
        
        reqBlockStatus = REQ_UNBLOCKUSER;
    }
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%d/%d/%d", SERVER_URL, REQ_SETBLOCKSTATUS, _user._idx, selectedUser._idx, reqBlockStatus];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        [APPDELEGATE hideLoadingView];
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            if(reqBlockStatus == REQ_BLOCKUSER) {
                
                [_user._blockList addObject:[NSNumber numberWithInt:selectedUser._idx]];
                [[JLToast makeText:SUCCESS_BLOCK duration:3] show];
                
//                [self.btnBlockorUnblock setTitle:@"unblock"];
                
            } else {
                
                [_user._blockList removeObject:[NSNumber numberWithInt:selectedUser._idx]];
                [[JLToast makeText:SUCCESS_UNBLOCK duration:3] show];
                
//                [self.btnBlockorUnblock setTitle:@"block"];
            }
            
        } else {
            
            if(reqBlockStatus == REQ_BLOCKUSER) {
                
                [[JLToast makeText:FAIL_BLOCK duration:3] show];
            } else {
                
                [[JLToast makeText:FAIL_UNBLOCK duration:3] show];
            }
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
    }];
}


- (IBAction) viewDidTapped:(id)sender {
    
    [self.inputbar resignFirstResponder];
}

-(IBAction)performBack:(id)sender {
    
    //do your saving and such here
    
    bIsShownChatView = NO;
    
    // update current coming message to old
    [[DBManager getSharedInstance] updateChatNoCurrentWithRoomName:chatRoom._name];
    
    // ******************
    // save last message of current chatting room
    // *******************
    if(messages.count > 0) {
        
        XmppPacket * _revPacket = messages[messages.count-1];
        
        // update current db ... _recentContent, _recentDate...
        // update with new info
        chatRoom._recentContent = _revPacket._bodyString;
        chatRoom._recentDate = _revPacket._sentTime;
        chatRoom._recentCounter = 0;                // no need.....
        
        // update db...
        // update db with _recentCounter....(only this field)
        [[DBManager getSharedInstance] updateRoomWithName:chatRoom._name recentCounter:0];
    }
    
    [APPDELEGATE.xmpp leaveRoomWithJID:chatRoom];
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
