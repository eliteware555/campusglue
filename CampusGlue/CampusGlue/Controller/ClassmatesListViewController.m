//
//  ClassmatesListViewController.m
//  CampusGlue
//
//  Created by victory on 3/20/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "ClassmatesListViewController.h"
#import "ClassCell.h"
#import "UserEntity.h"
#import "ReqConst.h"
#import "CommonUtils.h"
#import "FriendsViewController.h"

@interface ClassmatesListViewController() <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate> {
    
    UserEntity *_user;
    NSMutableArray *classList;
}

@property (nonatomic, weak) IBOutlet UITableView *tblClasses;

@end

@implementation ClassmatesListViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    _user = APPDELEGATE.Me;
    classList = [[NSMutableArray alloc] init];
    
    [self initView];
}

- (void) initView {
    
    self.navigationItem.title = @"The University Of Texas - Arlington";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void) didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self getClasses];
}

// ---------------------------------------------------------------------------
#pragma mark - UITableViewDataSource
// ---------------------------------------------------------------------------
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    
    // return the number of sections
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in section
    return [classList count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * cellIdentifier = @"ClassCell";
    
    ClassCell *cell = (ClassCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.lblClassName.text = classList[indexPath.row];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50;
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:@"SegueClassmates2Friends"]) {

        FriendsViewController *friendsVC = (FriendsViewController *)[segue destinationViewController];
        friendsVC.fromWhere = FROM_CLASSMATES;
        
        // selected indexpath, send a class name
        NSIndexPath *selectedpath = [_tblClasses indexPathForSelectedRow];
        
        friendsVC.className = classList[selectedpath.row];
    }
}

- (void) getClasses {
    
    [APPDELEGATE showLoadingView:self];
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%d", SERVER_URL, REQ_GETCLASS, _user._idx];
    NSString *  escapedURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:escapedURL parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        [APPDELEGATE hideLoadingView];
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            [classList removeAllObjects];
            
            NSArray * classes = [responseObject objectForKey:RES_CLASSNAME];
            
            for (int i = 0; i < [classes count]; i ++) {
                NSString *classname = classes[i];
                [classList addObject:classname];
            }
            
            [self.tblClasses reloadData];
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
    }];
}


@end


