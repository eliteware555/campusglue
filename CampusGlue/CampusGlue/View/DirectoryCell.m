//
//  DictionaryCell.m
//  CampusGlue
//
//  Created by victory on 3/18/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "DirectoryCell.h"
#import "CommonUtils.h"
#import "UserEntity.h"

@implementation DirectoryCell

@synthesize imvProfile, lblName, lblMajor;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void) setUserModel:(UserEntity *) _entity {
    
    [imvProfile setImageWithURL:[NSURL URLWithString:_entity._photoUrl] placeholderImage:[UIImage imageNamed:@"photo.png"]];
    
    lblName.text = [_entity getFullName];
    
//    NSLog(@"major: %d", _entity._major);
    
    lblMajor.text = [CommonUtils getMajors][_entity._major];
}

- (void) setRoomModel: (RoomEntity *) _roomEntity {
    
    UserEntity *_participant = _roomEntity._participants[0];
    [imvProfile setImageWithURL:[NSURL URLWithString:_participant._photoUrl] placeholderImage:[UIImage imageNamed:@"photo.png"]];
    

    lblName.text = _roomEntity._displayName;
    
    // chat content
    lblMajor.text = _roomEntity._recentContent;
}

@end
