//
//  ChatHeader.m
//  CampusGlue
//
//  Created by victory on 3/26/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "ChatHeader.h"

@implementation ChatHeader

@synthesize lblDate;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setDate: (NSString *)strDate {
    
    lblDate.text = strDate;
}

@end
