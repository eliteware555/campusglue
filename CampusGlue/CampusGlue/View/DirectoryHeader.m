//
//  DictionaryHeader.m
//  CampusGlue
//
//  Created by victory on 3/18/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "DirectoryHeader.h"

@implementation DirectoryHeader

@synthesize lblHeader;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void) setHeader:(NSString *) _header {

    lblHeader.text = _header;
}

@end
