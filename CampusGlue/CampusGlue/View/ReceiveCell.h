//
//  ReceiveCell.h
//  CampusGlue
//
//  Created by victory on 3/26/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XmppPacket.h"

@interface ReceiveCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblMessage;
@property (nonatomic, weak) IBOutlet UILabel *lblTime;

- (void) setMessage:(XmppPacket *) revPacket;

@end
