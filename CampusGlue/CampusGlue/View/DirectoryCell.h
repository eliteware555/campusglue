//
//  DictionaryCell.h
//  CampusGlue
//
//  Created by victory on 3/18/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserEntity.h"

@interface DirectoryCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *imvProfile;
@property (nonatomic, weak) IBOutlet UILabel *lblName;
@property (nonatomic, weak) IBOutlet UILabel *lblMajor;

- (void) setUserModel:(UserEntity *) _entity;
- (void) setRoomModel: (RoomEntity *) _roomEntity ;

@end
