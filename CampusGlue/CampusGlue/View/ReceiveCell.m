//
//  ReceiveCell.m
//  CampusGlue
//
//  Created by victory on 3/26/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "ReceiveCell.h"

@implementation ReceiveCell

@synthesize lblTime, lblMessage;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setMessage:(XmppPacket *) revPacket {
    
    lblTime.text = revPacket._sentTime;
    lblMessage.text = revPacket._bodyString;
}

@end
