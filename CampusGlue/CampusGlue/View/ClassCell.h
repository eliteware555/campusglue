//
//  ClassCell.h
//  CampusGlue
//
//  Created by victory on 3/20/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClassCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblClassName;

@end
