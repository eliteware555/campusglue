//
//  DictionaryHeader.h
//  CampusGlue
//
//  Created by victory on 3/18/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DirectoryHeader : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblHeader;

- (void) setHeader:(NSString *) _header;

@end
