//
//  ChatHeader.h
//  CampusGlue
//
//  Created by victory on 3/26/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatHeader : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblDate;

- (void) setDate: (NSString *)strDate;

@end
