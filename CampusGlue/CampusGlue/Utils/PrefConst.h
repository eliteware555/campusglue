//
//  PrefConst.h
//  DChat
//
//  Created by ChenJunLi on 12/17/15.
//  Copyright © 2015 Elite. All rights reserved.
//

// UserDefault Save Key

#ifndef PrefConst_h
#define PrefConst_h

#define PREFKEY_USEREMAIL                   @"useeremail"
#define PREFKEY_USERPWD                     @"userpassword"
#define PREFKEY_XMPPID                      @"XMPPID"

#define PREFKEY_LASTLOGINID                 @"lastlogin_id"





#define PREFKEY_USERNAME                    @"username"
#define PREFKEY_USER_ALLOW_PUSH               @"allowed_push_notification"
#define PREFKEY_USER_ALLOW_ADD_FRIEND          @"allowed_add_friend"

#define PREFKEY_LOGGED_IN                   @"logged_in"
#define PREFKEY_DEVICE_TOKEN                @"device_token"
#define PREFKEY_USERID                      @"userid"

#endif /* PrefConst_h */
