//
//  Constants.h
//  DChat
//
//  Created by ChenJunLi on 12/12/15.
//  Copyright © 2015 Elite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <JLToast/JLToast-Swift.h>
@import AFNetworking;
@import JLToast;

@interface Constants : NSObject

/**
 **     Constants define Macro
 **/
#pragma mark -
#pragma mark - constans define


#define PROFILE_IMG_SIZE 256
#define SAVE_ROOT_PATH                          @"Campus"

#define FROM_CHATLIST          0
#define FROM_MY_PROFILE         1

#define FROM_USER_PROFILE       2
#define FROM_CLASSMATES          3


/**
 **     Userdefaults and AppDelegate Macro
 **/
#pragma mark -
#pragma mark - UserDefaults and AppDelegate

#define USERDEFAULTS [NSUserDefaults standardUserDefaults]
#define APPDELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])

/**
 **  Constans Define Macro
 **/

#pragma mark -
#pragma mark - Constants Macro

// -------------------------------------------------------------
// string define
// -------------------------------------------------------------


#define   LOADING                   @"Connecting to the server. Please wait..."
#define   ALERT_OK                  @"Okay"
#define   ALERT_CANCEL              @"Cancel"
#define   NAV_TITLE                 @"The University Of Texas - Arlington"

#define   INPUT_FIRSTNAME           @"Please input your first name."
#define   INPUT_LASTNAME            @"Please input your last name."
#define   INPUT_EMAIL               @"Please input your UTA email."
#define   INPUT_PWD                 @"Please input your password."
#define   INPUT_CODE                @"Please input verification code."

#define   NOCLASS                   @"There is no class. Please add your class."

#define READ_TERMS                  @"Please read and accept the Terms and Privacy Policy."

#define   SELECT_MAJOR              @"Please select major."
#define   SELECT_YEAR               @"Please select year."
#define   SELECT_SEX                @"Please select sex."
#define   SELECT_STATUS             @"Please select relationship status."
#define   SELECT_INTEREST           @"Please select interested in."
#define   SELECT_PHOTO              @"Please add your photo."
#define   SELECT_MOVIE              @"Write your favorite movie."
#define   SELECT_ABOUTME            @"Write about you."

#define   CONN_ERROR                @"Connecting to the server failed.\nPlease try again."
#define   CHATTING_ERROR            @"The connection to the chat server has been shut down."
#define   CHATTING_SUCCCESS         @"It was connected to the chat server."
#define   CHATTING_CONNECTING       @"It is being connected to the chat server."
#define   EMAIL_EXIST               @"This email already exists."
#define   INVALIDEMAIL              @"Please enter a valid email address."
#define   NOT_AUTHENTICATE          @"E-mail has not been certified."
#define   PHOTO_UPLOAD_FAIL          @"Picture registration failed."
#define   PHOTO_UPLOAD_SUCCESS      @"The uploaded file was successful."
#define   UNREGISTERED_USER         @"Unregistered user."
#define   WRONG_AUTH                @"Please input verification code correctly."
#define   REGISTER_FAIL             @"Registration failed.\nTry again later."
#define   CHECK_PWD                 @"Please enter the password correctly."
#define   UPDATE_SUCCESS            @"Succeed to update profile."
#define   ADDFRIEND_SUCCESS         @"Friend request sent."
#define   UNFRIEND_SUCCESS          @"Succeed to unfriend."
#define   ADDFRIEND_FAIL            @"Failed to add friend."
#define   UNFRIEND_FAIL             @"Failed to unfriend."
#define   ADDCLASS_SUCCESS          @"Succeed to add class."
#define   CONFIRM_DELETECLASS       @"Do you want to delete this class?"
#define   RESETPWD_SUCCESS          @"Succeed to reset password."
#define   CHECK_CODE                @"Please check your mail, and input the verification code."

#define   SUCCESS_UNBLOCK           @"Succeed to unblocked"
#define   FAIL_UNBLOCK              @"Fail to unblock"
#define   SUCCESS_BLOCK             @"Succeed to blocked"
#define   FAIL_BLOCK                @"Fail to block"

#define   ADDFRIEND_MSG             @"just sent you a friend request."

#define   SUCCESS_REPORT            @"Succesfully reported."
#define   FAIL_REPORT               @"Sorry, try again later."

#define TIME_AM                                 @"AM"
#define TIME_PM                                 @"PM"

#define KEY_TIME_SEPERTATOR                     @"#"
#define KEY_FILE_MARKER                         @"FILE#"
#define KEY_IMAGE_MARKER                        @"IMAGE#"
#define KEY_VIDEO_MARKER                        @"VIDEO#"
#define KEY_ROOM_MARKER                         @"ROOM#"


#define MENU_BLOCK                              @"Block User"
#define MENU_UNBLOCK                            @"Unblock User"
#define MENU_REPORT                             @"Report User"

#define SelfColor(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]



extern BOOL bEnteredBackground;                 // when the app is entering in a background, it wil be set as a true.
                                                // after the app is entering in a foreground, it will be set as a false.

extern BOOL bIsShownChatView;                   // if the current topviewcontroller is ChatViewController, true

extern BOOL bIsShownChatList;                   // the current topviewcontroller is ChatListViewController, true 
@end









