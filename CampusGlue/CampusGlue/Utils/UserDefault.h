//
//  UserDefault.h
//  DChat
//
//  Created by ChenJunLi on 12/12/15.
//  Copyright © 2015 Elite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "PrefConst.h"
#import "AppDelegate.h"

@interface UserDefault : NSObject

// set and get method with string
+(void) setStringValue:(NSString *) keyString value:(NSString*) value;
+(NSString *) getStringValue:(NSString *) keyString;

// set and get method with bool
+(void) setBoolValue : (NSString *) keyString value:(BOOL) value;
+(BOOL) getBoolValue : (NSString *) keyString;

// set and get method with NSNumber(int, double, long)
+(void) setLongValue : (NSString *) keyString value:(NSNumber *) value;
+(NSNumber *) getLongValue : (NSString *) keyString;

// set and get method with int value
+(void) setIntValue:(NSString *) keyString value:(int) value;
+(int) getIntValue:(NSString *) keyString;
@end
