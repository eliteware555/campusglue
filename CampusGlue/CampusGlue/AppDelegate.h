//
//  AppDelegate.h
//  CampusGlue
//
//  Created by victory on 3/11/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PCAngularActivityIndicatorView.h"
#import "Reachability.h"
#import "ReqConst.h"
#import "UITabbar+CustomBadge.h"
#import "LNNotificationsUI.h"
#import "DBManager.h"
#import "UserEntity.h"
#import "XmppEndPoint.h"
#import "ChatViewController.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate> {

    UIView* baseView;
    
    UserEntity *Me;
    XmppEndPoint * xmpp;
    
    NSString * _deviceToken;
    
    UITabBar * gTabbar;
}


@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) PCAngularActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UserEntity *Me;
@property (nonatomic, strong) XmppEndPoint * xmpp;
@property (nonatomic, strong) NSString * _deviceToken;
@property (nonatomic, retain) UITabBar * gTabbar;

@property (nonatomic, strong) ChatViewController * chatVC;

- (void) showLoadingView:(id) sender;
- (void) hideLoadingView;
- (void) hideLoadingView: (NSTimeInterval) delay;

- (void) showAlertDialog :(NSString *)title message:(NSString *) message positive:(NSString *)strPositivie negative:(NSString *) strNegative sender:(id) sender;

+ (UIImage *)imageFromColor:(UIColor *)color forSize:(CGSize)size withCornerRadius:(CGFloat)radius;

// check the current network state
-(BOOL) connected;

- (void) notifyReceiveNewMessage;

@end

